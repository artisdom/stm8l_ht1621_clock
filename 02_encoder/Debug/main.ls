   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.12.6 - 16 Dec 2021
   3                     ; Generator (Limited) V4.5.4 - 16 Dec 2021
5174                     ; 8 main()
5174                     ; 9 {
5176                     	switch	.text
5177  0000               _main:
5179  0000 5203          	subw	sp,#3
5180       00000003      OFST:	set	3
5183                     ; 10   	int count=0;
5185                     ; 11 	uint8_t tick=0;
5187  0002 0f01          	clr	(OFST-2,sp)
5189                     ; 13     CLK_DIVR = 4;         	// SYSCLK=1MHz
5191  0004 350450c0      	mov	_CLK_DIVR,#4
5192                     ; 14 	CLK_PCKENR2 |= 0x02;    // Enable TIM1
5194  0008 721250c4      	bset	_CLK_PCKENR2,#1
5195                     ; 15 	CLK_PCKENR1 |= 0x01;	// Enable TIM2		
5197  000c 721050c3      	bset	_CLK_PCKENR1,#0
5198                     ; 19     PA_DDR = 0xFF; PA_CR1 = 0xFF; PA_ODR = 0;
5200  0010 35ff5002      	mov	_PA_DDR,#255
5203  0014 35ff5003      	mov	_PA_CR1,#255
5206  0018 725f5000      	clr	_PA_ODR
5207                     ; 21     PB_DDR = 0xFF; PB_CR1 = 0xFF; PB_ODR = 0;
5209  001c 35ff5007      	mov	_PB_DDR,#255
5212  0020 35ff5008      	mov	_PB_CR1,#255
5215  0024 725f5005      	clr	_PB_ODR
5216                     ; 23     PC_DDR = 0xFF; PC_CR1 = 0xFF; PC_ODR = 0;
5218  0028 35ff500c      	mov	_PC_DDR,#255
5221  002c 35ff500d      	mov	_PC_CR1,#255
5224  0030 725f500a      	clr	_PC_ODR
5225                     ; 27     PE_DDR = 0xFF; PE_CR1 = 0xFF; PE_ODR = 0;
5227  0034 35ff5016      	mov	_PE_DDR,#255
5230  0038 35ff5017      	mov	_PE_CR1,#255
5233  003c 725f5014      	clr	_PE_ODR
5234                     ; 29     PF_DDR = 0xFF; PF_CR1 = 0xFF; PF_ODR = 0;
5236  0040 35ff501b      	mov	_PF_DDR,#255
5239  0044 35ff501c      	mov	_PF_CR1,#255
5242  0048 725f5019      	clr	_PF_ODR
5243                     ; 33 	PB_DDR = ((1<<CS) | (1<<WR) | (1<<DATA)); // Push-Pull Mode
5245  004c 35705007      	mov	_PB_DDR,#112
5246                     ; 34     PB_CR1 = ((1<<CS) | (1<<WR) | (1<<DATA)); //
5248  0050 35705008      	mov	_PB_CR1,#112
5249                     ; 35     PB_CR2 = ((1<<CS) | (1<<WR) | (1<<DATA)); // Speed up 10 MHz
5251  0054 35705009      	mov	_PB_CR2,#112
5252                     ; 37 	PB_DDR &= ~((1<<ENC_L) | (1<<ENC_R));
5254  0058 c65007        	ld	a,_PB_DDR
5255  005b a4fa          	and	a,#250
5256  005d c75007        	ld	_PB_DDR,a
5257                     ; 38     PB_CR1 |=  ((1<<ENC_L) | (1<<ENC_R));
5259  0060 c65008        	ld	a,_PB_CR1
5260  0063 aa05          	or	a,#5
5261  0065 c75008        	ld	_PB_CR1,a
5262                     ; 41     TIM1_SR1   = 0x0;                       // Clear Pending Bit
5264  0068 725f52b6      	clr	_TIM1_SR1
5265                     ; 42     TIM1_CR1   = 0x0;                       // Clear TIM1_CR1
5267  006c 725f52b0      	clr	_TIM1_CR1
5268                     ; 43     TIM1_CR2   = 0x0;                       // Clear TIM1_CR2
5270  0070 725f52b1      	clr	_TIM1_CR2
5271                     ; 44     TIM1_PSCRH = 0x0;  TIM1_PSCRL = 3;     // Prescaler = 64
5273  0074 725f52c1      	clr	_TIM1_PSCRH
5276  0078 350352c2      	mov	_TIM1_PSCRL,#3
5277                     ; 45     TIM1_ARRH  = 0x7a; TIM1_ARRL  = 0x12;   // (10^6)/prescaler(=64) =31250 -> 0x7A12 -> freq Timer IRQ =1Hz
5279  007c 357a52c3      	mov	_TIM1_ARRH,#122
5282  0080 351252c4      	mov	_TIM1_ARRL,#18
5283                     ; 46     TIM1_IER   = 0x01;                      // set UIE flag, enable interrupt
5285  0084 350152b5      	mov	_TIM1_IER,#1
5286                     ; 47     TIM1_CR1  |= 0x01;                      // set CEN flag, start timer
5288  0088 721052b0      	bset	_TIM1_CR1,#0
5289                     ; 49 	TIM2_CCER1|= 0x22; 	// CC2P,CC1P: Capture/compare 2 output polarity
5291  008c c6525b        	ld	a,_TIM2_CCER1
5292  008f aa22          	or	a,#34
5293  0091 c7525b        	ld	_TIM2_CCER1,a
5294                     ; 50 	TIM2_CCMR1|= 0x01; 	// CC1 channel is configured as input, IC1 is mapped on TI1FP1
5296  0094 72105259      	bset	_TIM2_CCMR1,#0
5297                     ; 51 	TIM2_CCMR2|= 0x01;	// CC2 channel is configured as input, IC2 is mapped on TI2FP2
5299  0098 7210525a      	bset	_TIM2_CCMR2,#0
5300                     ; 52 	TIM2_SMCR |= 0x01; 	// encoder mode 1
5302  009c 72105252      	bset	_TIM2_SMCR,#0
5303                     ; 53 	TIM2_ARRH=1;		// max value counter =500
5305  00a0 3501525f      	mov	_TIM2_ARRH,#1
5306                     ; 54 	TIM2_ARRL=0xf4;		// max value counter =500
5308  00a4 35f45260      	mov	_TIM2_ARRL,#244
5309                     ; 55 	TIM2_CNTRH=0;		// clear counter
5311  00a8 725f525c      	clr	_TIM2_CNTRH
5312                     ; 56 	TIM2_CNTRL=0;		// clear counter
5314  00ac 725f525d      	clr	_TIM2_CNTRL
5315                     ; 57 	TIM2_CR1|=0x1; 		// enable counter
5317  00b0 72105250      	bset	_TIM2_CR1,#0
5318                     ; 59     ht1621_init();	
5320  00b4 cd0000        	call	_ht1621_init
5322                     ; 60     ht1621_clear();
5324  00b7 cd0000        	call	_ht1621_clear
5326                     ; 61 	ht1621_print(0);
5328  00ba 5f            	clrw	x
5329  00bb cd0000        	call	_ht1621_print
5331  00be               L7153:
5332                     ; 63 		if (tick == 20 ) {
5334  00be 7b01          	ld	a,(OFST-2,sp)
5335  00c0 a114          	cp	a,#20
5336  00c2 2608          	jrne	L3253
5337                     ; 64 		  PB_ODR ^=(1<<LED);
5339  00c4 90105005      	bcpl	_PB_ODR,#0
5340                     ; 65 		  tick=0;		  
5342  00c8 0f01          	clr	(OFST-2,sp)
5345  00ca 2002          	jra	L5253
5346  00cc               L3253:
5347                     ; 67 		  tick++;
5349  00cc 0c01          	inc	(OFST-2,sp)
5351  00ce               L5253:
5352                     ; 70 		count=(uint16_t)TIM2_CNTRH;
5354  00ce c6525c        	ld	a,_TIM2_CNTRH
5355  00d1 5f            	clrw	x
5356  00d2 97            	ld	xl,a
5357  00d3 1f02          	ldw	(OFST-1,sp),x
5359                     ; 71 		count = count <<8;
5361  00d5 7b03          	ld	a,(OFST+0,sp)
5362  00d7 6b02          	ld	(OFST-1,sp),a
5363  00d9 0f03          	clr	(OFST+0,sp)
5365                     ; 72 		count |=(uint16_t)TIM2_CNTRL;
5367  00db c6525d        	ld	a,_TIM2_CNTRL
5368  00de 5f            	clrw	x
5369  00df 97            	ld	xl,a
5370  00e0 01            	rrwa	x,a
5371  00e1 1a03          	or	a,(OFST+0,sp)
5372  00e3 01            	rrwa	x,a
5373  00e4 1a02          	or	a,(OFST-1,sp)
5374  00e6 01            	rrwa	x,a
5375  00e7 1f02          	ldw	(OFST-1,sp),x
5377                     ; 73 		count = count >>1;
5379  00e9 0702          	sra	(OFST-1,sp)
5380  00eb 0603          	rrc	(OFST+0,sp)
5382                     ; 74 		if (count >= 125) {
5384  00ed 9c            	rvf
5385  00ee 1e02          	ldw	x,(OFST-1,sp)
5386  00f0 a3007d        	cpw	x,#125
5387  00f3 2f07          	jrslt	L7253
5388                     ; 75 		  count = (count - 250);
5390  00f5 1e02          	ldw	x,(OFST-1,sp)
5391  00f7 1d00fa        	subw	x,#250
5392  00fa 1f02          	ldw	(OFST-1,sp),x
5394  00fc               L7253:
5395                     ; 78 		ht1621_print(count);
5397  00fc 1e02          	ldw	x,(OFST-1,sp)
5398  00fe cd0000        	call	_ht1621_print
5400                     ; 81 		_asm("wfi");
5403  0101 8f            wfi
5406  0102 20ba          	jra	L7153
5419                     	xdef	_main
5420                     	xref	_ht1621_init
5421                     	xref	_ht1621_clear
5422                     	xref	_ht1621_print
5441                     	end
