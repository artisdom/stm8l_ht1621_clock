                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.1.0 #12072 (Linux)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _main
                                     12 	.globl _delay_ms
                                     13 	.globl _tim4_irq
                                     14 	.globl _ht1621_print_num
                                     15 	.globl _ht1621_init
                                     16 	.globl _ht1621_clear
                                     17 	.globl _count
                                     18 ;--------------------------------------------------------
                                     19 ; ram data
                                     20 ;--------------------------------------------------------
                                     21 	.area DATA
      000000                         22 _count::
      000000                         23 	.ds 2
                                     24 ;--------------------------------------------------------
                                     25 ; ram data
                                     26 ;--------------------------------------------------------
                                     27 	.area INITIALIZED
                                     28 ;--------------------------------------------------------
                                     29 ; Stack segment in internal ram 
                                     30 ;--------------------------------------------------------
                                     31 	.area	SSEG
      000002                         32 __start__stack:
      000002                         33 	.ds	1
                                     34 
                                     35 ;--------------------------------------------------------
                                     36 ; absolute external ram data
                                     37 ;--------------------------------------------------------
                                     38 	.area DABS (ABS)
                                     39 
                                     40 ; default segment ordering for linker
                                     41 	.area HOME
                                     42 	.area GSINIT
                                     43 	.area GSFINAL
                                     44 	.area CONST
                                     45 	.area INITIALIZER
                                     46 	.area CODE
                                     47 
                                     48 ;--------------------------------------------------------
                                     49 ; interrupt vector 
                                     50 ;--------------------------------------------------------
                                     51 	.area HOME
      008000                         52 __interrupt_vect:
      008000 82 00 80 6B             53 	int s_GSINIT ; reset
      008004 82 00 00 00             54 	int 0x000000 ; trap
      008008 82 00 00 00             55 	int 0x000000 ; int0
      00800C 82 00 00 00             56 	int 0x000000 ; int1
      008010 82 00 00 00             57 	int 0x000000 ; int2
      008014 82 00 00 00             58 	int 0x000000 ; int3
      008018 82 00 00 00             59 	int 0x000000 ; int4
      00801C 82 00 00 00             60 	int 0x000000 ; int5
      008020 82 00 00 00             61 	int 0x000000 ; int6
      008024 82 00 00 00             62 	int 0x000000 ; int7
      008028 82 00 00 00             63 	int 0x000000 ; int8
      00802C 82 00 00 00             64 	int 0x000000 ; int9
      008030 82 00 00 00             65 	int 0x000000 ; int10
      008034 82 00 00 00             66 	int 0x000000 ; int11
      008038 82 00 00 00             67 	int 0x000000 ; int12
      00803C 82 00 00 00             68 	int 0x000000 ; int13
      008040 82 00 00 00             69 	int 0x000000 ; int14
      008044 82 00 00 00             70 	int 0x000000 ; int15
      008048 82 00 00 00             71 	int 0x000000 ; int16
      00804C 82 00 00 00             72 	int 0x000000 ; int17
      008050 82 00 00 00             73 	int 0x000000 ; int18
      008054 82 00 00 00             74 	int 0x000000 ; int19
      008058 82 00 00 00             75 	int 0x000000 ; int20
      00805C 82 00 00 00             76 	int 0x000000 ; int21
      008060 82 00 00 00             77 	int 0x000000 ; int22
      008064 82 00 80 92             78 	int _tim4_irq ; int23
                                     79 ;--------------------------------------------------------
                                     80 ; global & static initialisations
                                     81 ;--------------------------------------------------------
                                     82 	.area HOME
                                     83 	.area GSINIT
                                     84 	.area GSFINAL
                                     85 	.area GSINIT
      00806B                         86 __sdcc_init_data:
                                     87 ; stm8_genXINIT() start
      00806B AE 00 02         [ 2]   88 	ldw x, #l_DATA
      00806E 27 07            [ 1]   89 	jreq	00002$
      008070                         90 00001$:
      008070 72 4F FF FF      [ 1]   91 	clr (s_DATA - 1, x)
      008074 5A               [ 2]   92 	decw x
      008075 26 F9            [ 1]   93 	jrne	00001$
      008077                         94 00002$:
      008077 AE 00 00         [ 2]   95 	ldw	x, #l_INITIALIZER
      00807A 27 09            [ 1]   96 	jreq	00004$
      00807C                         97 00003$:
      00807C D6 80 91         [ 1]   98 	ld	a, (s_INITIALIZER - 1, x)
      00807F D7 00 01         [ 1]   99 	ld	(s_INITIALIZED - 1, x), a
      008082 5A               [ 2]  100 	decw	x
      008083 26 F7            [ 1]  101 	jrne	00003$
      008085                        102 00004$:
                                    103 ; stm8_genXINIT() end
                                    104 	.area GSFINAL
      008085 CC 80 68         [ 2]  105 	jp	__sdcc_program_startup
                                    106 ;--------------------------------------------------------
                                    107 ; Home
                                    108 ;--------------------------------------------------------
                                    109 	.area HOME
                                    110 	.area HOME
      008068                        111 __sdcc_program_startup:
      008068 CC 80 C6         [ 2]  112 	jp	_main
                                    113 ;	return from main will return to caller
                                    114 ;--------------------------------------------------------
                                    115 ; code
                                    116 ;--------------------------------------------------------
                                    117 	.area CODE
                                    118 ;	main.c: 8: void tim4_irq(void) __interrupt(23) {
                                    119 ;	-----------------------------------------
                                    120 ;	 function tim4_irq
                                    121 ;	-----------------------------------------
      008092                        122 _tim4_irq:
                                    123 ;	main.c: 9: if (count)
                                    124 ;	main.c: 10: --count;
      008092 CE 00 00         [ 2]  125 	ldw	x, _count+0
      008095 27 04            [ 1]  126 	jreq	00102$
      008097 5A               [ 2]  127 	decw	x
      008098 CF 00 00         [ 2]  128 	ldw	_count+0, x
      00809B                        129 00102$:
                                    130 ;	main.c: 12: TIM4_SR=0;
      00809B 35 00 53 44      [ 1]  131 	mov	0x5344+0, #0x00
                                    132 ;	main.c: 13: }
      00809F 80               [11]  133 	iret
                                    134 ;	main.c: 15: void delay_ms(uint16_t ms)
                                    135 ;	-----------------------------------------
                                    136 ;	 function delay_ms
                                    137 ;	-----------------------------------------
      0080A0                        138 _delay_ms:
                                    139 ;	main.c: 17: count = ms;
      0080A0 1E 03            [ 2]  140 	ldw	x, (0x03, sp)
      0080A2 CF 00 00         [ 2]  141 	ldw	_count+0, x
                                    142 ;	main.c: 18: TIM4_SR   = 0x0;                        // Clear Pending Bit
      0080A5 35 00 53 44      [ 1]  143 	mov	0x5344+0, #0x00
                                    144 ;	main.c: 19: TIM4_PSCR = TIM4_PRESCALER_128;         // =7, prescaler =128
      0080A9 35 07 53 47      [ 1]  145 	mov	0x5347+0, #0x07
                                    146 ;	main.c: 20: TIM4_ARR  = 15;                         // freq Timer IRQ ~1kHz
      0080AD 35 0F 53 48      [ 1]  147 	mov	0x5348+0, #0x0f
                                    148 ;	main.c: 21: TIM4_IER  = (uint8_t)TIM4_IT_UPDATE;    // =1, enable interrupt
      0080B1 35 01 53 43      [ 1]  149 	mov	0x5343+0, #0x01
                                    150 ;	main.c: 22: TIM4_CR1  = TIM4_CR1_CEN;               // =1, enable counter
      0080B5 35 01 53 40      [ 1]  151 	mov	0x5340+0, #0x01
                                    152 ;	main.c: 23: while(count) {
      0080B9                        153 00101$:
      0080B9 CE 00 00         [ 2]  154 	ldw	x, _count+0
      0080BC 27 03            [ 1]  155 	jreq	00103$
                                    156 ;	main.c: 24: wfi();                                  // goto sleep
      0080BE 8F               [10]  157 	wfi;	
      0080BF 20 F8            [ 2]  158 	jra	00101$
      0080C1                        159 00103$:
                                    160 ;	main.c: 26: TIM4_CR1  = 0x0;                        //  disable counter
      0080C1 35 00 53 40      [ 1]  161 	mov	0x5340+0, #0x00
                                    162 ;	main.c: 27: }
      0080C5 81               [ 4]  163 	ret
                                    164 ;	main.c: 29: int main() {
                                    165 ;	-----------------------------------------
                                    166 ;	 function main
                                    167 ;	-----------------------------------------
      0080C6                        168 _main:
      0080C6 52 02            [ 2]  169 	sub	sp, #2
                                    170 ;	main.c: 30: CLK_CKDIVR=(uint8_t)0x18;				// HSI= 2MHz
      0080C8 35 18 50 C6      [ 1]  171 	mov	0x50c6+0, #0x18
                                    172 ;	main.c: 32: PB_DDR|=(LED);
      0080CC 72 1A 50 07      [ 1]  173 	bset	20487, #5
                                    174 ;	main.c: 33: PB_CR1|=(LED);
      0080D0 C6 50 08         [ 1]  175 	ld	a, 0x5008
      0080D3 AA 20            [ 1]  176 	or	a, #0x20
      0080D5 C7 50 08         [ 1]  177 	ld	0x5008, a
                                    178 ;	main.c: 34: PC_DDR = ((1<<CS) | (1<<WR) | (1<<DATA)); // Push-Pull Mode
      0080D8 35 70 50 0C      [ 1]  179 	mov	0x500c+0, #0x70
                                    180 ;	main.c: 35: PC_CR1 = ((1<<CS) | (1<<WR) | (1<<DATA)); //
      0080DC 35 70 50 0D      [ 1]  181 	mov	0x500d+0, #0x70
                                    182 ;	main.c: 36: PC_CR2 = ((1<<CS) | (1<<WR) | (1<<DATA)); // Speed up 10 MHz
      0080E0 35 70 50 0E      [ 1]  183 	mov	0x500e+0, #0x70
                                    184 ;	main.c: 38: ht1621_init();
      0080E4 CD 81 1B         [ 4]  185 	call	_ht1621_init
                                    186 ;	main.c: 39: ht1621_clear();
      0080E7 CD 81 47         [ 4]  187 	call	_ht1621_clear
                                    188 ;	main.c: 40: enableInterrupts();
      0080EA 9A               [ 1]  189 	rim;	
                                    190 ;	main.c: 41: uint16_t tick=0;
      0080EB 5F               [ 1]  191 	clrw	x
      0080EC 1F 01            [ 2]  192 	ldw	(0x01, sp), x
      0080EE                        193 00102$:
                                    194 ;	main.c: 44: PB_ODR |= (LED);
      0080EE 72 1A 50 05      [ 1]  195 	bset	20485, #5
                                    196 ;	main.c: 45: delay_ms(500);
      0080F2 4B F4            [ 1]  197 	push	#0xf4
      0080F4 4B 01            [ 1]  198 	push	#0x01
      0080F6 CD 80 A0         [ 4]  199 	call	_delay_ms
      0080F9 5B 02            [ 2]  200 	addw	sp, #2
                                    201 ;	main.c: 46: PB_ODR &= ~(LED);
      0080FB 72 1B 50 05      [ 1]  202 	bres	20485, #5
                                    203 ;	main.c: 47: delay_ms(500);
      0080FF 4B F4            [ 1]  204 	push	#0xf4
      008101 4B 01            [ 1]  205 	push	#0x01
      008103 CD 80 A0         [ 4]  206 	call	_delay_ms
      008106 5B 02            [ 2]  207 	addw	sp, #2
                                    208 ;	main.c: 48: ht1621_print_num(tick++);
      008108 1E 01            [ 2]  209 	ldw	x, (0x01, sp)
      00810A 16 01            [ 2]  210 	ldw	y, (0x01, sp)
      00810C 90 5C            [ 1]  211 	incw	y
      00810E 17 01            [ 2]  212 	ldw	(0x01, sp), y
      008110 89               [ 2]  213 	pushw	x
      008111 CD 81 61         [ 4]  214 	call	_ht1621_print_num
      008114 5B 02            [ 2]  215 	addw	sp, #2
      008116 20 D6            [ 2]  216 	jra	00102$
                                    217 ;	main.c: 50: }
      008118 5B 02            [ 2]  218 	addw	sp, #2
      00811A 81               [ 4]  219 	ret
                                    220 	.area CODE
                                    221 	.area CONST
                                    222 	.area INITIALIZER
                                    223 	.area CABS (ABS)
