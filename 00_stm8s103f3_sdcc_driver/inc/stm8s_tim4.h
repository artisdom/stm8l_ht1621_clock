#ifndef __STM8S_TIM4_H
#define __STM8S_TIM4_H

#define TIM4_CR1_ARPE ((uint8_t)0x80) /*!< Auto-Reload Preload Enable mask. */
#define TIM4_CR1_OPM  ((uint8_t)0x08) /*!< One Pulse Mode mask. */
#define TIM4_CR1_URS  ((uint8_t)0x04) /*!< Update Request Source mask. */
#define TIM4_CR1_UDIS ((uint8_t)0x02) /*!< Update DIsable mask. */
#define TIM4_CR1_CEN  ((uint8_t)0x01) /*!< Counter Enable mask. */
/*IER*/
#define TIM4_IER_UIE  ((uint8_t)0x01) /*!< Update Interrupt Enable mask. */
/*SR1*/
#define TIM4_SR1_UIF  ((uint8_t)0x01) /*!< Update Interrupt Flag mask. */
/*EGR*/
#define TIM4_EGR_UG   ((uint8_t)0x01) /*!< Update Generation mask. */
/*CNTR*/
#define TIM4_CNTR_CNT ((uint8_t)0xFF) /*!< Counter Value (LSB) mask. */
/*PSCR*/
#define TIM4_PSCR_PSC ((uint8_t)0x07) /*!< Prescaler Value  mask. */
/*ARR*/
#define TIM4_ARR_ARR  ((uint8_t)0xFF) /*!< Autoreload Value mask. */

typedef enum
{
	TIM4_PRESCALER_1	= ((uint8_t)0x00),
	TIM4_PRESCALER_2    = ((uint8_t)0x01),
	TIM4_PRESCALER_4    = ((uint8_t)0x02),
	TIM4_PRESCALER_8	= ((uint8_t)0x03),
	TIM4_PRESCALER_16   = ((uint8_t)0x04),
	TIM4_PRESCALER_32   = ((uint8_t)0x05),
	TIM4_PRESCALER_64   = ((uint8_t)0x06),
	TIM4_PRESCALER_128  = ((uint8_t)0x07)
} TIM4_Prescaler_TypeDef;

/** TIM4 Flags */
typedef enum
{
  TIM4_FLAG_UPDATE                   = ((uint8_t)0x01)
}TIM4_FLAG_TypeDef;


/** TIM4 interrupt sources */
typedef enum
{
  TIM4_IT_UPDATE                     = ((uint8_t)0x01)
}TIM4_IT_TypeDef;

#endif
