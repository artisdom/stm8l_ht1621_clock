/* STM8S103F.h */

/* Copyright (c) 2003-2017 STMicroelectronics */

#ifndef __STM8S103F__
#define __STM8S103F__

#define enableInterrupts()  	__asm rim; __endasm     // enable interrupts
#define disableInterrupts()     __asm sim; __endasm;    // disable interrupts
#define rim()                   __asm rim; __endasm;    // enable interrupts
#define sim()                   __asm sim; __endasm;    // disable interrupts
#define nop()                   __asm nop; __endasm;    // No Operation
#define trap()                  __asm trap; __endasm;   // Trap (soft IT)
#define wfi()                   __asm wfi;  __endasm;   // Wait For Interrupt
#define halt()                  __asm halt; __endasm;   // Halt


/* Port A */
/*****************************************************************/

/* Port A data output latch register */
#define PA_ODR *(unsigned char*)0x5000

/* Port A input pin value register */
#define PA_IDR *(unsigned char*)0x5001

/* Port A data direction register */
#define PA_DDR *(unsigned char*)0x5002

/* Port A control register 1 */
#define PA_CR1 *(unsigned char*)0x5003

/* Port A control register 2 */
#define PA_CR2 *(unsigned char*)0x5004

/* Port B */
/*****************************************************************/

/* Port B data output latch register */
#define PB_ODR *(unsigned char*)0x5005

/* Port B input pin value register */
#define PB_IDR *(unsigned char*)0x5006

/* Port B data direction register */
#define PB_DDR *(unsigned char*)0x5007

/* Port B control register 1 */
#define PB_CR1 *(unsigned char*)0x5008

/* Port B control register 2 */
#define PB_CR2 *(unsigned char*)0x5009

/* Port C */
/*****************************************************************/

/* Port C data output latch register */
#define PC_ODR *(unsigned char*)0x500a

/* Port C input pin value register */
#define PC_IDR *(unsigned char*)0x500b

/* Port C data direction register */
#define PC_DDR *(unsigned char*)0x500c

/* Port C control register 1 */
#define PC_CR1 *(unsigned char*)0x500d

/* Port C control register 2 */
#define PC_CR2 *(unsigned char*)0x500e

/* Port D */
/*****************************************************************/

/* Port D data output latch register */
#define PD_ODR *(unsigned char*)0x500f

/* Port D input pin value register */
#define PD_IDR *(unsigned char*)0x5010

/* Port D data direction register */
#define PD_DDR *(unsigned char*)0x5011

/* Port D control register 1 */
#define PD_CR1 *(unsigned char*)0x5012

/* Port D control register 2 */
#define PD_CR2 *(unsigned char*)0x5013

/* Flash */
/*****************************************************************/

/* Flash control register 1 */
#define FLASH_CR1 *(unsigned char*)0x505a

/* Flash control register 2 */
#define FLASH_CR2 *(unsigned char*)0x505b

/* Flash complementary control register 2 */
#define FLASH_NCR2 *(unsigned char*)0x505c

/* Flash protection register */
#define FLASH_FPR *(unsigned char*)0x505d

/* Flash complementary protection register */
#define FLASH_NFPR *(unsigned char*)0x505e

/* Flash in-application programming status register */
#define FLASH_IAPSR *(unsigned char*)0x505f

/* Flash Program memory unprotection register */
#define FLASH_PUKR *(unsigned char*)0x5062

/* Data EEPROM unprotection register */
#define FLASH_DUKR *(unsigned char*)0x5064

/* External Interrupt Control Register (ITC) */
/*****************************************************************/

/* External interrupt control register 1 */
#define EXTI_CR1 *(unsigned char*)0x50a0

/* External interrupt control register 2 */
#define EXTI_CR2 *(unsigned char*)0x50a1

/* Reset (RST) */
/*****************************************************************/

/* Reset status register 1 */
#define RST_SR *(unsigned char*)0x50b3

/* Clock Control (CLK) */
/*****************************************************************/

/* Internal clock control register */
#define CLK_ICKR *(unsigned char*)0x50c0

/* External clock control register */
#define CLK_ECKR *(unsigned char*)0x50c1

/* Clock master status register */
#define CLK_CMSR *(unsigned char*)0x50c3

/* Clock master switch register */
#define CLK_SWR *(unsigned char*)0x50c4

/* Clock switch control register */
#define CLK_SWCR *(unsigned char*)0x50c5

/* Clock divider register */
#define CLK_CKDIVR *(unsigned char*)0x50c6

/* Peripheral clock gating register 1 */
#define CLK_PCKENR1 *(unsigned char*)0x50c7

/* Clock security system register */
#define CLK_CSSR *(unsigned char*)0x50c8

/* Configurable clock control register */
#define CLK_CCOR *(unsigned char*)0x50c9

/* Peripheral clock gating register 2 */
#define CLK_PCKENR2 *(unsigned char*)0x50ca

/* CAN clock control register */
#define CLK_CANCCR *(unsigned char*)0x50cb

/* HSI clock calibration trimming register */
#define CLK_HSITRIMR *(unsigned char*)0x50cc

/* SWIM clock control register */
#define CLK_SWIMCCR *(unsigned char*)0x50cd

/* Window Watchdog (WWDG) */
/*****************************************************************/

/* WWDG Control Register */
#define WWDG_CR *(unsigned char*)0x50d1

/* WWDR Window Register */
#define WWDG_WR *(unsigned char*)0x50d2

/* Independent Watchdog (IWDG) */
/*****************************************************************/

/* IWDG Key Register */
#define IWDG_KR *(unsigned char*)0x50e0

/* IWDG Prescaler Register */
#define IWDG_PR *(unsigned char*)0x50e1

/* IWDG Reload Register */
#define IWDG_RLR *(unsigned char*)0x50e2

/* Auto Wake-Up (AWU) */
/*****************************************************************/

/* AWU Control/Status Register */
#define AWU_CSR *(unsigned char*)0x50f0

/* AWU asynchronous prescaler buffer register */
#define AWU_APR *(unsigned char*)0x50f1

/* AWU Timebase selection register */
#define AWU_TBR *(unsigned char*)0x50f2

/* Beeper (BEEP) */
/*****************************************************************/

/* BEEP Control/Status Register */
#define BEEP_CSR *(unsigned char*)0x50f3

/* Serial Peripheral Interface (SPI) */
/*****************************************************************/

/* SPI Control Register 1 */
#define SPI_CR1 *(unsigned char*)0x5200

/* SPI Control Register 2 */
#define SPI_CR2 *(unsigned char*)0x5201

/* SPI Interrupt Control Register */
#define SPI_ICR *(unsigned char*)0x5202

/* SPI Status Register */
#define SPI_SR *(unsigned char*)0x5203

/* SPI Data Register */
#define SPI_DR *(unsigned char*)0x5204

/* SPI CRC Polynomial Register */
#define SPI_CRCPR *(unsigned char*)0x5205

/* SPI Rx CRC Register */
#define SPI_RXCRCR *(unsigned char*)0x5206

/* SPI Tx CRC Register */
#define SPI_TXCRCR *(unsigned char*)0x5207

/* I2C Bus Interface (I2C) */
/*****************************************************************/

/* I2C control register 1 */
#define I2C_CR1 *(unsigned char*)0x5210

/* I2C control register 2 */
#define I2C_CR2 *(unsigned char*)0x5211

/* I2C frequency register */
#define I2C_FREQR *(unsigned char*)0x5212

/* I2C Own address register low */
#define I2C_OARL *(unsigned char*)0x5213

/* I2C Own address register high */
#define I2C_OARH *(unsigned char*)0x5214

/* I2C data register */
#define I2C_DR *(unsigned char*)0x5216

/* I2C status register 1 */
#define I2C_SR1 *(unsigned char*)0x5217

/* I2C status register 2 */
#define I2C_SR2 *(unsigned char*)0x5218

/* I2C status register 3 */
#define I2C_SR3 *(unsigned char*)0x5219

/* I2C interrupt control register */
#define I2C_ITR *(unsigned char*)0x521a

/* I2C Clock control register low */
#define I2C_CCRL *(unsigned char*)0x521b

/* I2C Clock control register high */
#define I2C_CCRH *(unsigned char*)0x521c

/* I2C TRISE register */
#define I2C_TRISER *(unsigned char*)0x521d

/* I2C packet error checking register */
#define I2C_PECR *(unsigned char*)0x521e

/* Universal synch/asynch receiver transmitter (UART1) */
/*****************************************************************/

/* UART1 Status Register */
#define UART1_SR *(unsigned char*)0x5230

/* UART1 Data Register */
#define UART1_DR *(unsigned char*)0x5231

/* UART1 Baud Rate Register 1 */
#define UART1_BRR1 *(unsigned char*)0x5232

/* UART1 Baud Rate Register 2 */
#define UART1_BRR2 *(unsigned char*)0x5233

/* UART1 Control Register 1 */
#define UART1_CR1 *(unsigned char*)0x5234

/* UART1 Control Register 2 */
#define UART1_CR2 *(unsigned char*)0x5235

/* UART1 Control Register 3 */
#define UART1_CR3 *(unsigned char*)0x5236

/* UART1 Control Register 4 */
#define UART1_CR4 *(unsigned char*)0x5237

/* UART1 Control Register 5 */
#define UART1_CR5 *(unsigned char*)0x5238

/* UART1 Guard time Register */
#define UART1_GTR *(unsigned char*)0x5239

/* UART1 Prescaler Register */
#define UART1_PSCR *(unsigned char*)0x523a

/* 16-Bit Timer 1 (TIM1) */
/*****************************************************************/

/* TIM1 Control register 1 */
#define TIM1_CR1 *(unsigned char*)0x5250

/* TIM1 Control register 2 */
#define TIM1_CR2 *(unsigned char*)0x5251

/* TIM1 Slave Mode Control register */
#define TIM1_SMCR *(unsigned char*)0x5252

/* TIM1 external trigger register */
#define TIM1_ETR *(unsigned char*)0x5253

/* TIM1 Interrupt enable register */
#define TIM1_IER *(unsigned char*)0x5254

/* TIM1 Status register 1 */
#define TIM1_SR1 *(unsigned char*)0x5255

/* TIM1 Status register 2 */
#define TIM1_SR2 *(unsigned char*)0x5256

/* TIM1 Event Generation register */
#define TIM1_EGR *(unsigned char*)0x5257

/* TIM1 Capture/Compare mode register 1 */
#define TIM1_CCMR1 *(unsigned char*)0x5258

/* TIM1 Capture/Compare mode register 2 */
#define TIM1_CCMR2 *(unsigned char*)0x5259

/* TIM1 Capture/Compare mode register 3 */
#define TIM1_CCMR3 *(unsigned char*)0x525a

/* TIM1 Capture/Compare mode register 4 */
#define TIM1_CCMR4 *(unsigned char*)0x525b

/* TIM1 Capture/Compare enable register 1 */
#define TIM1_CCER1 *(unsigned char*)0x525c

/* TIM1 Capture/Compare enable register 2 */
#define TIM1_CCER2 *(unsigned char*)0x525d

/* TIM1 Counter */
#define TIM1_CNTR *(unsigned int*)0x525e
/* Data bits High */
#define TIM1_CNTRH *(unsigned char*)0x525e
/* Data bits Low */
#define TIM1_CNTRL *(unsigned char*)0x525f

/* TIM1 Prescaler register */
#define TIM1_PSCR *(unsigned int*)0x5260
/* Data bits High */
#define TIM1_PSCRH *(unsigned char*)0x5260
/* Data bits Low */
#define TIM1_PSCRL *(unsigned char*)0x5261

/* TIM1 Auto-reload register */
#define TIM1_ARR *(unsigned int*)0x5262
/* Data bits High */
#define TIM1_ARRH *(unsigned char*)0x5262
/* Data bits Low */
#define TIM1_ARRL *(unsigned char*)0x5263

/* TIM1 Repetition counter register */
#define TIM1_RCR *(unsigned char*)0x5264

/* TIM1 Capture/Compare register 1 */
#define TIM1_CCR1 *(unsigned int*)0x5265
/* Data bits High */
#define TIM1_CCR1H *(unsigned char*)0x5265
/* Data bits Low */
#define TIM1_CCR1L *(unsigned char*)0x5266

/* TIM1 Capture/Compare register 2 */
#define TIM1_CCR2 *(unsigned int*)0x5267
/* Data bits High */
#define TIM1_CCR2H *(unsigned char*)0x5267
/* Data bits Low */
#define TIM1_CCR2L *(unsigned char*)0x5268

/* TIM1 Capture/Compare register 3 */
#define TIM1_CCR3 *(unsigned int*)0x5269
/* Data bits High */
#define TIM1_CCR3H *(unsigned char*)0x5269
/* Data bits Low */
#define TIM1_CCR3L *(unsigned char*)0x526a

/* TIM1 Capture/Compare register 4 */
#define TIM1_CCR4 *(unsigned int*)0x526b
/* Data bits High */
#define TIM1_CCR4H *(unsigned char*)0x526b
/* Data bits Low */
#define TIM1_CCR4L *(unsigned char*)0x526c

/* TIM1 Break register */
#define TIM1_BKR *(unsigned char*)0x526d

/* TIM1 Dead-time register */
#define TIM1_DTR *(unsigned char*)0x526e

/* TIM1 Output idle state register */
#define TIM1_OISR *(unsigned char*)0x526f

/* 16-Bit Timer 2 (TIM2) */
/*****************************************************************/

/* TIM2 Control register 1 */
#define TIM2_CR1 *(unsigned char*)0x5300

/* TIM2 Interrupt enable register */
#define TIM2_IER *(unsigned char*)0x5303

/* TIM2 Status register 1 */
#define TIM2_SR1 *(unsigned char*)0x5304

/* TIM2 Status register 2 */
#define TIM2_SR2 *(unsigned char*)0x5305

/* TIM2 Event Generation register */
#define TIM2_EGR *(unsigned char*)0x5306

/* TIM2 Capture/Compare mode register 1 */
#define TIM2_CCMR1 *(unsigned char*)0x5307

/* TIM2 Capture/Compare mode register 2 */
#define TIM2_CCMR2 *(unsigned char*)0x5308

/* TIM2 Capture/Compare mode register 3 */
#define TIM2_CCMR3 *(unsigned char*)0x5309

/* TIM2 Capture/Compare enable register 1 */
#define TIM2_CCER1 *(unsigned char*)0x530a

/* TIM2 Capture/Compare enable register 2 */
#define TIM2_CCER2 *(unsigned char*)0x530b

/* TIM2 Counter */
#define TIM2_CNTR *(unsigned int*)0x530c
/* Data bits High */
#define TIM2_CNTRH *(unsigned char*)0x530c
/* Data bits Low */
#define TIM2_CNTRL *(unsigned char*)0x530d

/* TIM2 Prescaler register */
#define TIM2_PSCR *(unsigned char*)0x530e

/* TIM2 Auto-reload register */
#define TIM2_ARR *(unsigned int*)0x530f
/* Data bits High */
#define TIM2_ARRH *(unsigned char*)0x530f
/* Data bits Low */
#define TIM2_ARRL *(unsigned char*)0x5310

/* TIM2 Capture/Compare register 1 */
#define TIM2_CCR1 *(unsigned int*)0x5311
/* Data bits High */
#define TIM2_CCR1H *(unsigned char*)0x5311
/* Data bits Low */
#define TIM2_CCR1L *(unsigned char*)0x5312

/* TIM2 Capture/Compare register 2 */
#define TIM2_CCR2 *(unsigned int*)0x5313
/* Data bits High */
#define TIM2_CCR2H *(unsigned char*)0x5313
/* Data bits Low */
#define TIM2_CCR2L *(unsigned char*)0x5314

/* TIM2 Capture/Compare register 3 */
#define TIM2_CCR3 *(unsigned int*)0x5315
/* Data bits High */
#define TIM2_CCR3H *(unsigned char*)0x5315
/* Data bits Low */
#define TIM2_CCR3L *(unsigned char*)0x5316

/* 8-Bit  Timer 4 (TIM4) */
/*****************************************************************/

/* TIM4 Control register 1 */
#define TIM4_CR1 *(unsigned char*)0x5340

/* TIM4 Interrupt enable register */
#define TIM4_IER *(unsigned char*)0x5343

/* TIM4 Status register */
#define TIM4_SR *(unsigned char*)0x5344

/* TIM4 Event Generation register */
#define TIM4_EGR *(unsigned char*)0x5345

/* TIM4 Counter */
#define TIM4_CNTR *(unsigned char*)0x5346

/* TIM4 Prescaler register */
#define TIM4_PSCR *(unsigned char*)0x5347

/* TIM4 Auto-reload register */
#define TIM4_ARR *(unsigned char*)0x5348

/* 10-Bit A/D Converter (ADC1) */
/*****************************************************************/

/* ADC Data buffer Register 0 */
#define ADC_DB0R *(unsigned int*)0x53e0
/* Data Buffer register 0 High */
#define ADC_DB0RH *(unsigned char*)0x53e0
/* Data Buffer register 0 Low */
#define ADC_DB0RL *(unsigned char*)0x53e1

/* ADC Data buffer Register 1 */
#define ADC_DB1R *(unsigned int*)0x53e2
/* Data Buffer register 1 High */
#define ADC_DB1RH *(unsigned char*)0x53e2
/* Data Buffer register 1 Low */
#define ADC_DB1RL *(unsigned char*)0x53e3

/* ADC Data buffer Register 2 */
#define ADC_DB2R *(unsigned int*)0x53e4
/* Data Buffer register 2 High */
#define ADC_DB2RH *(unsigned char*)0x53e4
/* Data Buffer register 2 Low */
#define ADC_DB2RL *(unsigned char*)0x53e5

/* ADC Data buffer Register 3 */
#define ADC_DB3R *(unsigned int*)0x53e6
/* Data Buffer register 3 High */
#define ADC_DB3RH *(unsigned char*)0x53e6
/* Data Buffer register 3 Low */
#define ADC_DB3RL *(unsigned char*)0x53e7

/* ADC Data buffer Register 4 */
#define ADC_DB4R *(unsigned int*)0x53e8
/* Data Buffer register 4 High */
#define ADC_DB4RH *(unsigned char*)0x53e8
/* Data Buffer register 4 Low */
#define ADC_DB4RL *(unsigned char*)0x53e9

/* ADC Data buffer Register 5 */
#define ADC_DB5R *(unsigned int*)0x53ea
/* Data Buffer register 5 High */
#define ADC_DB5RH *(unsigned char*)0x53ea
/* Data Buffer register 5 Low */
#define ADC_DB5RL *(unsigned char*)0x53eb

/* ADC Data buffer Register 6 */
#define ADC_DB6R *(unsigned int*)0x53ec
/* Data Buffer register 6 High */
#define ADC_DB6RH *(unsigned char*)0x53ec
/* Data Buffer register 6 Low */
#define ADC_DB6RL *(unsigned char*)0x53ed

/* ADC Data buffer Register 7 */
#define ADC_DB7R *(unsigned int*)0x53ee
/* Data Buffer register 7 High */
#define ADC_DB7RH *(unsigned char*)0x53ee
/* Data Buffer register 7 Low */
#define ADC_DB7RL *(unsigned char*)0x53ef

/* ADC Data buffer Register 8 */
#define ADC_DB8R *(unsigned int*)0x53f0
/* Data Buffer register 8 High */
#define ADC_DB8RH *(unsigned char*)0x53f0
/* Data Buffer register 8 Low */
#define ADC_DB8RL *(unsigned char*)0x53f1

/* ADC Data buffer Register 9 */
#define ADC_DB9R *(unsigned int*)0x53f2
/* Data Buffer register 9 High */
#define ADC_DB9RH *(unsigned char*)0x53f2
/* Data Buffer register 9 Low */
#define ADC_DB9RL *(unsigned char*)0x53f3

/* ADC Control/Status Register */
#define ADC_CSR *(unsigned char*)0x5400

/* ADC Configuration Register 1 */
#define ADC_CR1 *(unsigned char*)0x5401

/* ADC Configuration Register 2 */
#define ADC_CR2 *(unsigned char*)0x5402

/* ADC Configuration Register 3 */
#define ADC_CR3 *(unsigned char*)0x5403

/* ADC Data Register */
#define ADC_DR *(unsigned int*)0x5404
/* Data bits High */
#define ADC_DRH *(unsigned char*)0x5404
/* Data bits Low */
#define ADC_DRL *(unsigned char*)0x5405

/* ADC Schmitt Trigger Disable Register */
#define ADC_TDR *(unsigned int*)0x5406
/* Schmitt trigger disable High */
#define ADC_TDRH *(unsigned char*)0x5406
/* Schmitt trigger disable Low */
#define ADC_TDRL *(unsigned char*)0x5407

/* ADC High Threshold Register */
#define ADC_HTR *(unsigned int*)0x5408
/* High Threshold Register High */
#define ADC_HTRH *(unsigned char*)0x5408
/* High Threshold Register Low */
#define ADC_HTRL *(unsigned char*)0x5409

/* ADC Low Threshold Register */
#define ADC_LTR *(unsigned int*)0x540a
/* Low Threshold Register High */
#define ADC_LTRH *(unsigned char*)0x540a
/* Low Threshold Register Low */
#define ADC_LTRL *(unsigned char*)0x540b

/* ADC Analog Watchdog Status Register */
#define ADC_AWSR *(unsigned int*)0x540c
/* Analog Watchdog Status register High */
#define ADC_AWSRH *(unsigned char*)0x540c
/* Analog Watchdog Status register Low */
#define ADC_AWSRL *(unsigned char*)0x540d

/* ADC Analog Watchdog Control Register */
#define ADC_AWCR *(unsigned int*)0x540e
/* Analog Watchdog Control register High */
#define ADC_AWCRH *(unsigned char*)0x540e
/* Analog Watchdog Control register Low */
#define ADC_AWCRL *(unsigned char*)0x540f

/*  Global configuration register (CFG) */
/*****************************************************************/

/* CFG Global configuration register */
#define CFG_GCR *(unsigned char*)0x7f60

/* Interrupt Software Priority Register (ITC) */
/*****************************************************************/

/* Interrupt Software priority register 1 */
#define ITC_SPR1 *(unsigned char*)0x7f70

/* Interrupt Software priority register 2 */
#define ITC_SPR2 *(unsigned char*)0x7f71

/* Interrupt Software priority register 3 */
#define ITC_SPR3 *(unsigned char*)0x7f72

/* Interrupt Software priority register 4 */
#define ITC_SPR4 *(unsigned char*)0x7f73

/* Interrupt Software priority register 5 */
#define ITC_SPR5 *(unsigned char*)0x7f74

/* Interrupt Software priority register 6 */
#define ITC_SPR6 *(unsigned char*)0x7f75

/* Interrupt Software priority register 7 */
#define ITC_SPR7 *(unsigned char*)0x7f76

#endif /* __STM8S103F__ */
