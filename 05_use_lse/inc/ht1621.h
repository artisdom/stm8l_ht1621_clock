#ifndef __HT1621_H__
#define __HT1621_H__

#define  WR	   5					// Clock signal
#define  CS	   4					// [C]hip [S]elect
#define  DATA  6					// MOSI

void main_loop(void);
void ht1621_minute(uint16_t t, uint8_t flag);
void ht1621_hour(uint16_t t, uint8_t flag);
void ht1621_time(uint16_t t, uint8_t flag);
void ht1621_print(int num);
void ht1621_clear(void);
void ht1621_init(void);
void ht1621_send_digit(uint8_t pos,uint8_t digit);
#endif 		// __HT1621_H__