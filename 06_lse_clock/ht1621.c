#include <stdint.h>
#include "iostm8l152x.h"
#include "ht1621.h"

#define num_len 10
#define MINUS	0x02

#define get_abs(n) ((n) < 0 ? -(n) : (n))

#define  BIAS     0x52             //0b1000 0101 0010  1/3duty 4com
#define  SYSDIS   0X00             //0b1000 0000 0000  ???????LCD?????
#define  SYSEN    0X02             //0b1000 0000 0010 ???????
#define  LCDOFF   0X04             //0b1000 0000 0100  ?LCD??
#define  LCDON    0X06             //0b1000 0000 0110  ??LCD??
#define  XTAL     0x28             //0b1000 0010 1000 ?????
#define  RC256    0X30             //0b1000 0011 0000  ????
#define  TONEON   0X12             //0b1000 0001 0010  ??????
#define  TONEOFF  0X10             //0b1000 0001 0000 ??????
#define  WDTDIS1  0X0A             //0b1000 0000 1010  ?????
#define  BUFFERSIZE 12

static uint8_t digits[10] = {0x7d,0x60,0x3e,0x7a,0x63,0x5b,0x5f,0x70,0x7f,0x7b};
uint8_t menu=0;
volatile int count=0;
volatile int offset=0;
	
void ht1621_send_cmd(uint8_t cmd);
void ht1621_send(uint8_t data, uint8_t len);
void ht1621_send_data(uint8_t adr, uint8_t value);
uint8_t ht1621_char_to_seg_bits(uint8_t ch);

void ht1621_init(void) {
    ht1621_send_cmd(BIAS);
    ht1621_send_cmd(RC256);
    ht1621_send_cmd(SYSDIS);
    ht1621_send_cmd(WDTDIS1);
    ht1621_send_cmd(SYSEN);
    ht1621_send_cmd(LCDON);
}

void ht1621_off() {
	ht1621_send_cmd(LCDOFF);
}

void ht1621_clear(void) {
    char i;
	for(i=0;i<16;i++) {
		ht1621_send_data((i<<1),0x0);
	}
}


/*
void ht1621_send_digit(uint8_t pos,uint8_t digit) {
	ht1621_send_data((pos<<1),digits[digit]);
}

void ht1621_minute(uint16_t t, uint8_t flag) {
  if (flag) {
	ht1621_time(t,flag);
	return;
  }
  
  t=t/60;
  ////////////////////
  PB_ODR &= ~(1<<CS);
  ht1621_send(0xa0, 3);
  ht1621_send(0, 6);
  ht1621_send(0, 8);
  ht1621_send(0, 8);
  ht1621_send(MINUS, 8);
  ht1621_send(digits[(t%10)], 8);
  t=t/10;
  if (t>0)
	ht1621_send(digits[(t)], 8);
  else 
	ht1621_send(0,8);      
  PB_ODR |= (1<<CS);
  //////////////////////
}

void ht1621_hour(uint16_t t, uint8_t flag) {
  uint8_t d;
  d=(uint8_t)(t%60);
  if (flag) {
	ht1621_time(t,flag);
	return;
  }
  ////////////////////
  PB_ODR &= ~(1<<CS);
  ht1621_send(0xa0, 3);
  ht1621_send(0, 6);
  ht1621_send(digits[(d%10)], 8);
  ht1621_send(digits[(d/10)], 8);
  ht1621_send(MINUS, 8);
  ht1621_send(0, 8);
  ht1621_send(0, 8);      
  PB_ODR |= (1<<CS);
  //////////////////////
}
*/

#pragma section(FLASH_CODE)
static void ht1621_time(uint16_t t, uint8_t flag) {
  uint8_t d,z;
  d=(uint8_t)(t%60);
  z=(uint8_t)(d%10);
  ////////////////////
  PB_ODR &= ~(1<<CS);
  ht1621_send(0xa0, 3);
  ht1621_send(0, 6);
  t=t/60;
  if (menu == (uint8_t)0x1 && !flag) {
	ht1621_send(0, 8);
	ht1621_send(0, 8);	
  }else {
	ht1621_send(digits[z], 8);
	z=(uint8_t)(d/10);
	ht1621_send(digits[z], 8);
  }
  if (flag || menu)
	ht1621_send(MINUS, 8);
  else 
	ht1621_send(0,8);

  if (menu == (uint8_t)0x2 && !flag) {
	ht1621_send(0, 8);
	ht1621_send(0, 8);	
  }else {  
	z=(uint8_t)(t%10);
	ht1621_send(digits[z], 8);
	z=(uint8_t)(t/10);
	if (t>0)
	  ht1621_send(digits[z], 8);
	else 
	  ht1621_send(0,8);   
  }	  
  PB_ODR |= (1<<CS);
  //////////////////////
}

void ht1621_print(int num) {

    uint16_t value=get_abs(num);
    uint8_t first=1;
	
	if (num <= 0) 
	  ht1621_clear();
	  
	PB_ODR &= ~(1<<CS);
	do {
        uint8_t rm=value % 10;

		if (first) {
		  ht1621_send(0xa0, 3);
		  ht1621_send(0, 6);
		  ht1621_send(digits[rm], 8);		 
		  first=0;
		} else {
		  ht1621_send(digits[rm], 8);		 
		}
        value=value/10;
    } while (value>0);  
	
	if (num < 0) 
	  ht1621_send(MINUS,8);  

	  
	PB_ODR |=  (1<<CS);
}

void ht1621_send_data(uint8_t adr, uint8_t value) {
	adr <<=2;
    PB_ODR &= ~(1<<CS);
    ht1621_send(0xa0, 3);
    ht1621_send(adr, 6);
    ht1621_send(value, 8);
    PB_ODR |=  (1<<CS);
}

void ht1621_send_cmd(uint8_t cmd) {
	PB_ODR &= ~(1<<CS);
	ht1621_send(0x80,4);
	ht1621_send(cmd,8);
	PB_ODR |=  (1<<CS);
}

void ht1621_send(uint8_t data, uint8_t len) {
	char i;
	for(i=0;i<len;i++) {
        PB_ODR=(data & 0x80) ? PB_ODR | (1<<DATA) : PB_ODR & ~(1<<DATA);
		PB_ODR &= ~(1<<WR);
		PB_ODR |= (1<<WR);
        data=(data<<1);
	}
}


void main_loop(void) {
	uint8_t prev=0;
	uint8_t btn_press=0;
	uint16_t i=40;
	uint8_t btn=0;	
    uint16_t tick=0;
	uint16_t time=1000;
	uint8_t blink=0;
	ht1621_init();	
    ht1621_clear();  
	//ht1621_time(time,0);
    // Switch to LSE (32,768 kHz)
    CLK_SWCR |= (1<<1);                     // set SWEN flag
    CLK_ECKCR |= (1<<2);                    // set LSEON flag
    while (!(CLK_ECKCR & 0x08));            // wait LSERDY flag
    CLK_SWR= 0x08;                          // set LSE
	while (CLK_SWCR & 0x01);				// wait to reset SWBSY flag
    CLK_ICKCR &= ~(1<<0);                   // reset HSION flag
	CLK_DIVR = 0;         					// SYSCLK=LSE
    //==== FLASH Switch Off =======
    FLASH_CR1 |= (1<<3);                    // set EEPM flag
    while (--i > 0);                        // wait after switch off flash memory
    // === MVR Swtch Off ==========
    CLK_REGCSR |= (1<<1);                   // set REGOFF flag
	// === main loop =========
	//FLASH_CR1 |= (1<<2); // set WAITM flag
	TIM4_CR1  |= 0x01;
	ht1621_time(time,0);
    for(;;) { 
	  // if press button		
	  btn=PB_IDR;
	  btn &= 0x2;
	  if (btn == 0) {
		menu = (menu<2) ? menu +1 : 0;
		TIM2_CNTRL=0;
		TIM2_CNTRH=0;				
		
		do {
		  btn=PB_IDR;
		  btn &= 0x2;		  
		  for(i=0; i<300;i++) {
			_asm("nop");
		  }
		} while (btn == 0);
	  }

	  // get encoder data
	  if (menu) {
		count=(uint16_t)TIM2_CNTRH;
		count = count <<8;
		count |=(uint16_t)TIM2_CNTRL;
		count = count >>1;
	  
		if (count >= 125) {
		  count = (count - 250);
		}
	  }

	  if (menu==2 && prev == 1) {
		time =offset;	
		time=time;
	  } else if (menu == 0 && prev == 2){
		TIM4_CR1&=~(0x01);	// disable timer
		TIM4_PSCR  = 0x0c;
		TIM4_CNTR=0;
		TIM4_CR1 |= (0x01);	// enable timer
		time =offset;
		tick=0;
	  } else if (menu == 1 && prev == 0) {
		TIM4_CR1&=~(0x01);	// disable timer
		TIM4_PSCR  = 0x0b;
		TIM4_CR1 |= (0x01);	// enable timer
		tick=0;
	  }


	  if (!(++tick%4)) {		// last 1 sec
		blink=!blink;
		switch (menu) {
		  case 2:
		    offset = time/60;
			offset += count;
			offset = get_abs(offset);
			offset = (offset<<4)-offset;
			offset <<=2;
			offset += (time%60);			
			ht1621_time(offset,blink);
			tick=0;
			break;
		  case 1:
			offset = time+count;			
			ht1621_time(offset,blink);
			tick=0;
			break;
		  default:
			ht1621_time(time,blink);	
		}
	  }
	  
		

	  // time count	  
	  if (!menu) {	  
		if (tick == 240) {		// if 1 minute

		  ++time;
		  tick=0;
		}		
		if (time == 1440)		// if 24 hour
		  time=0;		  
	  }//	 else 
		  //ht1621_time(time,blink);			 	  
	  
	  _asm("wfe");
	  TIM4_SR1 =0;	

	  prev=menu; 
    }
}
#pragma section()