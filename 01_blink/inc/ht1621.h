#ifndef __HT1621_H__
#define __HT1621_H__

#define  BIAS     0x52             //0b1000 0101 0010  1/3duty 4com
#define  SYSDIS   0X00             //0b1000 0000 0000  ???????LCD?????
#define  SYSEN    0X02             //0b1000 0000 0010 ???????
#define  LCDOFF   0X04             //0b1000 0000 0100  ?LCD??
#define  LCDON    0X06             //0b1000 0000 0110  ??LCD??
#define  XTAL     0x28             //0b1000 0010 1000 ?????
#define  RC256    0X30             //0b1000 0011 0000  ????
#define  TONEON   0X12             //0b1000 0001 0010  ??????
#define  TONEOFF  0X10             //0b1000 0001 0000 ??????
#define  WDTDIS1  0X0A             //0b1000 0000 1010  ?????
#define  BUFFERSIZE 12

#define  WR	   5					// Clock signal
#define  CS	   4					// [C]hip [S]elect
#define  DATA  6					// MOSI

void ht1621_print(uint16_t num);
void ht1621_clear(void);
void ht1621_init(void);
void ht1621_send_digit(uint8_t pos,uint8_t digit);
void ht1621_print_num(uint16_t num);
#endif 		// __HT1621_H__