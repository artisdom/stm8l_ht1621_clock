   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.12.6 - 16 Dec 2021
   3                     ; Generator (Limited) V4.5.4 - 16 Dec 2021
5247                     ; 8 main()
5247                     ; 9 {
5249                     	switch	.text
5250  0000               _main:
5252  0000 520d          	subw	sp,#13
5253       0000000d      OFST:	set	13
5256                     ; 10 	int count=0;
5258  0002 5f            	clrw	x
5259  0003 1f0a          	ldw	(OFST-3,sp),x
5261                     ; 11     int offset=0;
5263  0005 5f            	clrw	x
5264  0006 1f0c          	ldw	(OFST-1,sp),x
5266                     ; 12 	uint8_t btn=0;		
5268                     ; 13 	uint8_t menu=0;
5270  0008 0f09          	clr	(OFST-4,sp)
5272                     ; 14 	uint8_t prev=menu;
5274  000a 0f02          	clr	(OFST-11,sp)
5276                     ; 15 	uint8_t blink=0;
5278  000c 0f03          	clr	(OFST-10,sp)
5280                     ; 16 	uint16_t tick=0;	
5282  000e 5f            	clrw	x
5283  000f 1f04          	ldw	(OFST-9,sp),x
5285                     ; 17 	uint16_t time=1000;
5287  0011 ae03e8        	ldw	x,#1000
5288  0014 1f07          	ldw	(OFST-6,sp),x
5290                     ; 18 	uint8_t btn_press=0;
5292                     ; 20     CLK_DIVR = 4;         	// SYSCLK=1MHz
5294  0016 350450c0      	mov	_CLK_DIVR,#4
5295                     ; 21 	CLK_PCKENR1 |= 0x02;    // Enable TIM3
5297  001a 721250c3      	bset	_CLK_PCKENR1,#1
5298                     ; 22 	CLK_PCKENR1 |= 0x01;	// Enable TIM2		
5300  001e 721050c3      	bset	_CLK_PCKENR1,#0
5301                     ; 26     PA_DDR = 0xFF; PA_CR1 = 0xFF; PA_ODR = 0;
5303  0022 35ff5002      	mov	_PA_DDR,#255
5306  0026 35ff5003      	mov	_PA_CR1,#255
5309  002a 725f5000      	clr	_PA_ODR
5310                     ; 28     PB_DDR = 0xFF; PB_CR1 = 0xFF; PB_ODR = 0;
5312  002e 35ff5007      	mov	_PB_DDR,#255
5315  0032 35ff5008      	mov	_PB_CR1,#255
5318  0036 725f5005      	clr	_PB_ODR
5319                     ; 30     PC_DDR = 0xFF; PC_CR1 = 0xFF; PC_ODR = 0;
5321  003a 35ff500c      	mov	_PC_DDR,#255
5324  003e 35ff500d      	mov	_PC_CR1,#255
5327  0042 725f500a      	clr	_PC_ODR
5328                     ; 32     PD_DDR = 0xFF; PD_CR1 = 0xFF; PD_ODR = 0;
5330  0046 35ff5011      	mov	_PD_DDR,#255
5333  004a 35ff5012      	mov	_PD_CR1,#255
5336  004e 725f500f      	clr	_PD_ODR
5337                     ; 34     PE_DDR = 0xFF; PE_CR1 = 0xFF; PE_ODR = 0;
5339  0052 35ff5016      	mov	_PE_DDR,#255
5342  0056 35ff5017      	mov	_PE_CR1,#255
5345  005a 725f5014      	clr	_PE_ODR
5346                     ; 36     PF_DDR = 0xFF; PF_CR1 = 0xFF; PF_ODR = 0;
5348  005e 35ff501b      	mov	_PF_DDR,#255
5351  0062 35ff501c      	mov	_PF_CR1,#255
5354  0066 725f5019      	clr	_PF_ODR
5355                     ; 40 	PB_DDR = ((1<<CS) | (1<<WR) | (1<<DATA)); // Push-Pull Mode
5357  006a 35705007      	mov	_PB_DDR,#112
5358                     ; 41     PB_CR1 = ((1<<CS) | (1<<WR) | (1<<DATA)); //
5360  006e 35705008      	mov	_PB_CR1,#112
5361                     ; 42     PB_CR2 = ((1<<CS) | (1<<WR) | (1<<DATA)); // Speed up 10 MHz
5363  0072 35705009      	mov	_PB_CR2,#112
5364                     ; 44 	PB_DDR &= ~((1<<ENC_L) | (1<<ENC_R));
5366  0076 c65007        	ld	a,_PB_DDR
5367  0079 a4fa          	and	a,#250
5368  007b c75007        	ld	_PB_DDR,a
5369                     ; 45     PB_CR1 |=  ((1<<ENC_L) | (1<<ENC_R));
5371  007e c65008        	ld	a,_PB_CR1
5372  0081 aa05          	or	a,#5
5373  0083 c75008        	ld	_PB_CR1,a
5374                     ; 47 	PB_DDR &= ~(1<<ENC_BTN);    // pullup
5376  0086 72135007      	bres	_PB_DDR,#1
5377                     ; 48     PB_CR1 |= (1<<ENC_BTN);
5379  008a 72125008      	bset	_PB_CR1,#1
5380                     ; 51     TIM3_SR1   = 0x0;                       // Clear Pending Bit
5382  008e 725f5286      	clr	_TIM3_SR1
5383                     ; 52     TIM3_CR1   = 0x0;                       // Clear TIM1_CR1
5385  0092 725f5280      	clr	_TIM3_CR1
5386                     ; 53     TIM3_CR2   = 0x0;                       // Clear TIM1_CR2
5388  0096 725f5281      	clr	_TIM3_CR2
5389                     ; 54     TIM3_PSCR  = 6;     					// Prescaler 2^6= 64
5391  009a 3506528e      	mov	_TIM3_PSCR,#6
5392                     ; 55     TIM3_ARRH  = 0x0c; TIM3_ARRL  = 0x35;   // (10^6)/64(prescaler) =15625 -> 0x7A12 -> freq Timer IRQ =1Hz
5394  009e 350c528f      	mov	_TIM3_ARRH,#12
5397  00a2 35355290      	mov	_TIM3_ARRL,#53
5398                     ; 56     TIM3_IER   = 0x01;                      // set UIE flag, enable interrupt
5400  00a6 35015285      	mov	_TIM3_IER,#1
5401                     ; 57 	TIM3_CNTRH=0;							// clear counter
5403  00aa 725f528c      	clr	_TIM3_CNTRH
5404                     ; 58 	TIM3_CNTRL=0;							// clear counter
5406  00ae 725f528d      	clr	_TIM3_CNTRL
5407                     ; 59 	WFE_CR3   |= 0x1;						// enable event
5409  00b2 721050a8      	bset	_WFE_CR3,#0
5410                     ; 60     TIM3_CR1  |= 0x01;                      // set CEN flag, start timer	
5412  00b6 72105280      	bset	_TIM3_CR1,#0
5413                     ; 62 	TIM2_CCER1|= 0x22; 	// CC2P,CC1P: Capture/compare 2 output polarity
5415  00ba c6525b        	ld	a,_TIM2_CCER1
5416  00bd aa22          	or	a,#34
5417  00bf c7525b        	ld	_TIM2_CCER1,a
5418                     ; 63 	TIM2_CCMR1|= 0x01; 	// CC1 channel is configured as input, IC1 is mapped on TI1FP1
5420  00c2 72105259      	bset	_TIM2_CCMR1,#0
5421                     ; 64 	TIM2_CCMR2|= 0x01;	// CC2 channel is configured as input, IC2 is mapped on TI2FP2
5423  00c6 7210525a      	bset	_TIM2_CCMR2,#0
5424                     ; 65 	TIM2_SMCR |= 0x01; 	// encoder mode 1
5426  00ca 72105252      	bset	_TIM2_SMCR,#0
5427                     ; 66 	TIM2_ARRH=1;		// max value counter =500
5429  00ce 3501525f      	mov	_TIM2_ARRH,#1
5430                     ; 67 	TIM2_ARRL=0xf4;		// max value counter =500
5432  00d2 35f45260      	mov	_TIM2_ARRL,#244
5433                     ; 68 	TIM2_CNTRH=0;		// clear counter
5435  00d6 725f525c      	clr	_TIM2_CNTRH
5436                     ; 69 	TIM2_CNTRL=0;		// clear counter
5438  00da 725f525d      	clr	_TIM2_CNTRL
5439                     ; 70 	TIM2_CR1|=0x1; 		// enable counter
5441  00de 72105250      	bset	_TIM2_CR1,#0
5442                     ; 72     ht1621_init();	
5444  00e2 cd0000        	call	_ht1621_init
5446                     ; 73     ht1621_clear();
5448  00e5 cd0000        	call	_ht1621_clear
5450                     ; 74 	ht1621_print(0);
5452  00e8 5f            	clrw	x
5453  00e9 cd0000        	call	_ht1621_print
5455                     ; 75 	FLASH_CR1 |= (1<<2); // set WAITM flag
5457  00ec 72145050      	bset	_FLASH_CR1,#2
5458  00f0               L1653:
5459                     ; 78 	  btn=PB_IDR;
5461  00f0 c65006        	ld	a,_PB_IDR
5462  00f3 6b06          	ld	(OFST-7,sp),a
5464                     ; 79 	  btn &= 0x2;
5466  00f5 7b06          	ld	a,(OFST-7,sp)
5467  00f7 a402          	and	a,#2
5468  00f9 6b06          	ld	(OFST-7,sp),a
5470                     ; 80 	  if (btn == 0) {
5472  00fb 0d06          	tnz	(OFST-7,sp)
5473  00fd 262b          	jrne	L5653
5474                     ; 81 		menu = (menu<2) ? menu +1 : 0;
5476  00ff 7b09          	ld	a,(OFST-4,sp)
5477  0101 a102          	cp	a,#2
5478  0103 2405          	jruge	L6
5479  0105 7b09          	ld	a,(OFST-4,sp)
5480  0107 4c            	inc	a
5481  0108 2001          	jra	L01
5482  010a               L6:
5483  010a 4f            	clr	a
5484  010b               L01:
5485  010b 6b09          	ld	(OFST-4,sp),a
5487                     ; 82 		TIM2_CNTRL=0;
5489  010d 725f525d      	clr	_TIM2_CNTRL
5490                     ; 83 		TIM2_CNTRH=0;				
5492  0111 725f525c      	clr	_TIM2_CNTRH
5493  0115               L7653:
5494                     ; 87 		  btn=PB_IDR;
5496  0115 c65006        	ld	a,_PB_IDR
5497  0118 6b06          	ld	(OFST-7,sp),a
5499                     ; 88 		  btn &= 0x2;		  
5501  011a 7b06          	ld	a,(OFST-7,sp)
5502  011c a402          	and	a,#2
5503  011e 6b06          	ld	(OFST-7,sp),a
5505                     ; 89 		  delay_ms(300);	  
5507  0120 ae012c        	ldw	x,#300
5508  0123 cd0000        	call	_delay_ms
5510                     ; 90 		} while (btn == 0);
5512  0126 0d06          	tnz	(OFST-7,sp)
5513  0128 27eb          	jreq	L7653
5514  012a               L5653:
5515                     ; 94 	  if (menu) {
5517  012a 0d09          	tnz	(OFST-4,sp)
5518  012c 272e          	jreq	L5753
5519                     ; 95 		count=(uint16_t)TIM2_CNTRH;
5521  012e c6525c        	ld	a,_TIM2_CNTRH
5522  0131 5f            	clrw	x
5523  0132 97            	ld	xl,a
5524  0133 1f0a          	ldw	(OFST-3,sp),x
5526                     ; 96 		count = count <<8;
5528  0135 7b0b          	ld	a,(OFST-2,sp)
5529  0137 6b0a          	ld	(OFST-3,sp),a
5530  0139 0f0b          	clr	(OFST-2,sp)
5532                     ; 97 		count |=(uint16_t)TIM2_CNTRL;
5534  013b c6525d        	ld	a,_TIM2_CNTRL
5535  013e 5f            	clrw	x
5536  013f 97            	ld	xl,a
5537  0140 01            	rrwa	x,a
5538  0141 1a0b          	or	a,(OFST-2,sp)
5539  0143 01            	rrwa	x,a
5540  0144 1a0a          	or	a,(OFST-3,sp)
5541  0146 01            	rrwa	x,a
5542  0147 1f0a          	ldw	(OFST-3,sp),x
5544                     ; 98 		count = count >>1;
5546  0149 070a          	sra	(OFST-3,sp)
5547  014b 060b          	rrc	(OFST-2,sp)
5549                     ; 100 		if (count >= 125) {
5551  014d 9c            	rvf
5552  014e 1e0a          	ldw	x,(OFST-3,sp)
5553  0150 a3007d        	cpw	x,#125
5554  0153 2f07          	jrslt	L5753
5555                     ; 101 		  count = (count - 250);
5557  0155 1e0a          	ldw	x,(OFST-3,sp)
5558  0157 1d00fa        	subw	x,#250
5559  015a 1f0a          	ldw	(OFST-3,sp),x
5561  015c               L5753:
5562                     ; 105 	  if (menu==2 && prev == 1) {
5564  015c 7b09          	ld	a,(OFST-4,sp)
5565  015e a102          	cp	a,#2
5566  0160 260c          	jrne	L1063
5568  0162 7b02          	ld	a,(OFST-11,sp)
5569  0164 a101          	cp	a,#1
5570  0166 2606          	jrne	L1063
5571                     ; 106 		time =offset;	
5573  0168 1e0c          	ldw	x,(OFST-1,sp)
5574  016a 1f07          	ldw	(OFST-6,sp),x
5577  016c 2050          	jra	L3063
5578  016e               L1063:
5579                     ; 107 	  } else if (menu == 0 && prev == 2){
5581  016e 0d09          	tnz	(OFST-4,sp)
5582  0170 2627          	jrne	L5063
5584  0172 7b02          	ld	a,(OFST-11,sp)
5585  0174 a102          	cp	a,#2
5586  0176 2621          	jrne	L5063
5587                     ; 108 		TIM3_CR1&=~(0x01);	// disable timer
5589  0178 72115280      	bres	_TIM3_CR1,#0
5590                     ; 109 		TIM3_ARRH  = 0x0c; 
5592  017c 350c528f      	mov	_TIM3_ARRH,#12
5593                     ; 110 		TIM3_ARRL  = 0x35;
5595  0180 35355290      	mov	_TIM3_ARRL,#53
5596                     ; 111 		TIM3_CNTRH=0;		// clear counter
5598  0184 725f528c      	clr	_TIM3_CNTRH
5599                     ; 112 		TIM3_CNTRL=0;		// clear counter
5601  0188 725f528d      	clr	_TIM3_CNTRL
5602                     ; 113 		TIM3_CR1 |= (0x01);	// enable timer
5604  018c 72105280      	bset	_TIM3_CR1,#0
5605                     ; 114 		time =offset;
5607  0190 1e0c          	ldw	x,(OFST-1,sp)
5608  0192 1f07          	ldw	(OFST-6,sp),x
5610                     ; 115 		tick=0;
5612  0194 5f            	clrw	x
5613  0195 1f04          	ldw	(OFST-9,sp),x
5616  0197 2025          	jra	L3063
5617  0199               L5063:
5618                     ; 116 	  } else if (menu == 1 && prev == 0) {
5620  0199 7b09          	ld	a,(OFST-4,sp)
5621  019b a101          	cp	a,#1
5622  019d 261f          	jrne	L3063
5624  019f 0d02          	tnz	(OFST-11,sp)
5625  01a1 261b          	jrne	L3063
5626                     ; 117 		TIM3_CR1&=~(0x01);	// disable timer
5628  01a3 72115280      	bres	_TIM3_CR1,#0
5629                     ; 118 		TIM3_ARRH  = 0x03; 
5631  01a7 3503528f      	mov	_TIM3_ARRH,#3
5632                     ; 119 		TIM3_ARRL  = 0xe8;
5634  01ab 35e85290      	mov	_TIM3_ARRL,#232
5635                     ; 120 		TIM3_CNTRH=0;		// clear counter
5637  01af 725f528c      	clr	_TIM3_CNTRH
5638                     ; 121 		TIM3_CNTRL=0;		// clear counter
5640  01b3 725f528d      	clr	_TIM3_CNTRL
5641                     ; 122 		TIM3_CR1 |= (0x01);	// enable timer
5643  01b7 72105280      	bset	_TIM3_CR1,#0
5644                     ; 123 		tick=0;
5646  01bb 5f            	clrw	x
5647  01bc 1f04          	ldw	(OFST-9,sp),x
5649  01be               L3063:
5650                     ; 127 	  if (!(++tick%5)) {		// last 1 sec
5652  01be 1e04          	ldw	x,(OFST-9,sp)
5653  01c0 1c0001        	addw	x,#1
5654  01c3 1f04          	ldw	(OFST-9,sp),x
5656  01c5 a605          	ld	a,#5
5657  01c7 62            	div	x,a
5658  01c8 5f            	clrw	x
5659  01c9 97            	ld	xl,a
5660  01ca a30000        	cpw	x,#0
5661  01cd 2703cc024f    	jrne	L3163
5662                     ; 128 		blink=!blink;
5664  01d2 0d03          	tnz	(OFST-10,sp)
5665  01d4 2604          	jrne	L21
5666  01d6 a601          	ld	a,#1
5667  01d8 2001          	jra	L41
5668  01da               L21:
5669  01da 4f            	clr	a
5670  01db               L41:
5671  01db 6b03          	ld	(OFST-10,sp),a
5673                     ; 129 		switch (menu) {
5675  01dd 7b09          	ld	a,(OFST-4,sp)
5677                     ; 144 		  default:
5677                     ; 145 			ht1621_time(time,blink);			
5678  01df 4a            	dec	a
5679  01e0 270e          	jreq	L7643
5680  01e2 4a            	dec	a
5681  01e3 2757          	jreq	L1743
5682  01e5               L3743:
5685  01e5 7b03          	ld	a,(OFST-10,sp)
5686  01e7 88            	push	a
5687  01e8 1e08          	ldw	x,(OFST-5,sp)
5688  01ea cd0000        	call	_ht1621_time
5690  01ed 84            	pop	a
5691  01ee 205f          	jra	L3163
5692  01f0               L7643:
5693                     ; 130 		  case 1:
5693                     ; 131 		    offset = count * 60;
5695  01f0 1e0a          	ldw	x,(OFST-3,sp)
5696  01f2 a63c          	ld	a,#60
5697  01f4 cd0000        	call	c_bmulx
5699  01f7 1f0c          	ldw	(OFST-1,sp),x
5701                     ; 132 			offset += (time/60)*60;
5703  01f9 1e07          	ldw	x,(OFST-6,sp)
5704  01fb a63c          	ld	a,#60
5705  01fd 62            	div	x,a
5706  01fe a63c          	ld	a,#60
5707  0200 cd0000        	call	c_bmulx
5709  0203 72fb0c        	addw	x,(OFST-1,sp)
5710  0206 1f0c          	ldw	(OFST-1,sp),x
5712                     ; 133 			offset = get_abs(offset);			
5714  0208 9c            	rvf
5715  0209 1e0c          	ldw	x,(OFST-1,sp)
5716  020b 2e05          	jrsge	L61
5717  020d 1e0c          	ldw	x,(OFST-1,sp)
5718  020f 50            	negw	x
5719  0210 2002          	jra	L02
5720  0212               L61:
5721  0212 1e0c          	ldw	x,(OFST-1,sp)
5722  0214               L02:
5723  0214 1f0c          	ldw	(OFST-1,sp),x
5725                     ; 134 			offset += time%60;
5727  0216 1e07          	ldw	x,(OFST-6,sp)
5728  0218 a63c          	ld	a,#60
5729  021a 62            	div	x,a
5730  021b 5f            	clrw	x
5731  021c 97            	ld	xl,a
5732  021d 72fb0c        	addw	x,(OFST-1,sp)
5733  0220 1f0c          	ldw	(OFST-1,sp),x
5735                     ; 135 			offset %= 1440;
5737  0222 1e0c          	ldw	x,(OFST-1,sp)
5738  0224 90ae05a0      	ldw	y,#1440
5739  0228 cd0000        	call	c_idiv
5741  022b 51            	exgw	x,y
5742  022c 1f0c          	ldw	(OFST-1,sp),x
5744                     ; 136 			ht1621_hour(offset,blink);
5746  022e 7b03          	ld	a,(OFST-10,sp)
5747  0230 88            	push	a
5748  0231 1e0d          	ldw	x,(OFST+0,sp)
5749  0233 cd0000        	call	_ht1621_hour
5751  0236 84            	pop	a
5752                     ; 137 			tick=0;
5754  0237 5f            	clrw	x
5755  0238 1f04          	ldw	(OFST-9,sp),x
5757                     ; 138 			break;
5759  023a 2013          	jra	L3163
5760  023c               L1743:
5761                     ; 139 		  case 2:
5761                     ; 140 			offset = time+count;			
5763  023c 1e0a          	ldw	x,(OFST-3,sp)
5764  023e 72fb07        	addw	x,(OFST-6,sp)
5765  0241 1f0c          	ldw	(OFST-1,sp),x
5767                     ; 141 			ht1621_minute(offset,blink);
5769  0243 7b03          	ld	a,(OFST-10,sp)
5770  0245 88            	push	a
5771  0246 1e0d          	ldw	x,(OFST+0,sp)
5772  0248 cd0000        	call	_ht1621_minute
5774  024b 84            	pop	a
5775                     ; 142 			tick=0;
5777  024c 5f            	clrw	x
5778  024d 1f04          	ldw	(OFST-9,sp),x
5780                     ; 143 			break;
5782  024f               L7163:
5783  024f               L3163:
5784                     ; 150 	  if (!menu) {	  
5786  024f 0d09          	tnz	(OFST-4,sp)
5787  0251 261b          	jrne	L1263
5788                     ; 151 		if (tick == 300) {		// if 1 minute
5790  0253 1e04          	ldw	x,(OFST-9,sp)
5791  0255 a3012c        	cpw	x,#300
5792  0258 260a          	jrne	L3263
5793                     ; 152 		  ++time;
5795  025a 1e07          	ldw	x,(OFST-6,sp)
5796  025c 1c0001        	addw	x,#1
5797  025f 1f07          	ldw	(OFST-6,sp),x
5799                     ; 153 		  tick=0;
5801  0261 5f            	clrw	x
5802  0262 1f04          	ldw	(OFST-9,sp),x
5804  0264               L3263:
5805                     ; 155 		if (time == 1440)		// if 24 hour
5807  0264 1e07          	ldw	x,(OFST-6,sp)
5808  0266 a305a0        	cpw	x,#1440
5809  0269 2603          	jrne	L1263
5810                     ; 156 		  time=0;		  
5812  026b 5f            	clrw	x
5813  026c 1f07          	ldw	(OFST-6,sp),x
5815  026e               L1263:
5816                     ; 160 	  prev=menu;
5818  026e 7b09          	ld	a,(OFST-4,sp)
5819  0270 6b02          	ld	(OFST-11,sp),a
5821                     ; 161 	  _asm("wfe");
5824  0272 728f          wfe
5826                     ; 162 	  TIM3_SR1 =0;
5828  0274 725f5286      	clr	_TIM3_SR1
5830  0278 acf000f0      	jpf	L1653
5843                     	xdef	_main
5844                     	xref	_delay_ms
5845                     	xref	_ht1621_init
5846                     	xref	_ht1621_clear
5847                     	xref	_ht1621_print
5848                     	xref	_ht1621_time
5849                     	xref	_ht1621_hour
5850                     	xref	_ht1621_minute
5851                     	xref.b	c_x
5870                     	xref	c_idiv
5871                     	xref	c_bmulx
5872                     	end
