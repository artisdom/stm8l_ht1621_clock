   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.12.6 - 16 Dec 2021
   3                     ; Generator (Limited) V4.5.4 - 16 Dec 2021
5087                     .const:	section	.text
5088  0000               _digits:
5089  0000 7d            	dc.b	125
5090  0001 60            	dc.b	96
5091  0002 3e            	dc.b	62
5092  0003 7a            	dc.b	122
5093  0004 63            	dc.b	99
5094  0005 5b            	dc.b	91
5095  0006 5f            	dc.b	95
5096  0007 70            	dc.b	112
5097  0008 7f            	dc.b	127
5098  0009 7b            	dc.b	123
5128                     ; 15 void ht1621_init(void) {
5130                     	switch	.text
5131  0000               _ht1621_init:
5135                     ; 16     ht1621_send_cmd(BIAS);
5137  0000 a652          	ld	a,#82
5138  0002 cd026a        	call	_ht1621_send_cmd
5140                     ; 17     ht1621_send_cmd(RC256);
5142  0005 a630          	ld	a,#48
5143  0007 cd026a        	call	_ht1621_send_cmd
5145                     ; 18     ht1621_send_cmd(SYSDIS);
5147  000a 4f            	clr	a
5148  000b cd026a        	call	_ht1621_send_cmd
5150                     ; 19     ht1621_send_cmd(WDTDIS1);
5152  000e a60a          	ld	a,#10
5153  0010 cd026a        	call	_ht1621_send_cmd
5155                     ; 20     ht1621_send_cmd(SYSEN);
5157  0013 a602          	ld	a,#2
5158  0015 cd026a        	call	_ht1621_send_cmd
5160                     ; 21     ht1621_send_cmd(LCDON);
5162  0018 a606          	ld	a,#6
5163  001a cd026a        	call	_ht1621_send_cmd
5165                     ; 22 }
5168  001d 81            	ret
5192                     ; 24 void ht1621_off() {
5193                     	switch	.text
5194  001e               _ht1621_off:
5198                     ; 25 	ht1621_send_cmd(LCDOFF);
5200  001e a604          	ld	a,#4
5201  0020 cd026a        	call	_ht1621_send_cmd
5203                     ; 26 }
5206  0023 81            	ret
5241                     ; 28 void ht1621_clear(void) {
5242                     	switch	.text
5243  0024               _ht1621_clear:
5245  0024 88            	push	a
5246       00000001      OFST:	set	1
5249                     ; 30 	for(i=0;i<16;i++) {
5251  0025 0f01          	clr	(OFST+0,sp)
5253  0027               L3353:
5254                     ; 31 		ht1621_send_data((i<<1),0x0);
5256  0027 7b01          	ld	a,(OFST+0,sp)
5257  0029 48            	sll	a
5258  002a 5f            	clrw	x
5259  002b 95            	ld	xh,a
5260  002c cd0246        	call	_ht1621_send_data
5262                     ; 30 	for(i=0;i<16;i++) {
5264  002f 0c01          	inc	(OFST+0,sp)
5268  0031 7b01          	ld	a,(OFST+0,sp)
5269  0033 a110          	cp	a,#16
5270  0035 25f0          	jrult	L3353
5271                     ; 33 }
5274  0037 84            	pop	a
5275  0038 81            	ret
5328                     ; 35 void ht1621_print_num(uint16_t num){
5329                     	switch	.text
5330  0039               _ht1621_print_num:
5332  0039 89            	pushw	x
5333  003a 89            	pushw	x
5334       00000002      OFST:	set	2
5337                     ; 36 	uint8_t pos=0;
5339  003b 0f02          	clr	(OFST+0,sp)
5341  003d               L7653:
5342                     ; 38         uint8_t rm=num % 10;
5344  003d 1e03          	ldw	x,(OFST+1,sp)
5345  003f a60a          	ld	a,#10
5346  0041 62            	div	x,a
5347  0042 5f            	clrw	x
5348  0043 97            	ld	xl,a
5349  0044 01            	rrwa	x,a
5350  0045 6b01          	ld	(OFST-1,sp),a
5351  0047 02            	rlwa	x,a
5353                     ; 39 		ht1621_send_digit(pos++,rm);
5355  0048 7b01          	ld	a,(OFST-1,sp)
5356  004a 97            	ld	xl,a
5357  004b 7b02          	ld	a,(OFST+0,sp)
5358  004d 0c02          	inc	(OFST+0,sp)
5360  004f 95            	ld	xh,a
5361  0050 ad0e          	call	_ht1621_send_digit
5363                     ; 40         num=num/10;
5365  0052 1e03          	ldw	x,(OFST+1,sp)
5366  0054 a60a          	ld	a,#10
5367  0056 62            	div	x,a
5368  0057 1f03          	ldw	(OFST+1,sp),x
5369                     ; 41     } while (num>0);
5371  0059 1e03          	ldw	x,(OFST+1,sp)
5372  005b 26e0          	jrne	L7653
5373                     ; 42 }
5376  005d 5b04          	addw	sp,#4
5377  005f 81            	ret
5422                     ; 44 void ht1621_send_digit(uint8_t pos,uint8_t digit) {
5423                     	switch	.text
5424  0060               _ht1621_send_digit:
5426  0060 89            	pushw	x
5427       00000000      OFST:	set	0
5430                     ; 45 	ht1621_send_data((pos<<1),digits[digit]);
5432  0061 9f            	ld	a,xl
5433  0062 5f            	clrw	x
5434  0063 97            	ld	xl,a
5435  0064 d60000        	ld	a,(_digits,x)
5436  0067 97            	ld	xl,a
5437  0068 7b01          	ld	a,(OFST+1,sp)
5438  006a 48            	sll	a
5439  006b 95            	ld	xh,a
5440  006c cd0246        	call	_ht1621_send_data
5442                     ; 46 }
5445  006f 85            	popw	x
5446  0070 81            	ret
5493                     ; 48 void ht1621_minute(uint16_t t, uint8_t flag) {
5494                     	switch	.text
5495  0071               _ht1621_minute:
5497  0071 89            	pushw	x
5498       00000000      OFST:	set	0
5501                     ; 49   if (flag) {
5503  0072 0d05          	tnz	(OFST+5,sp)
5504  0074 2709          	jreq	L1463
5505                     ; 50 	ht1621_time(t,flag);
5507  0076 7b05          	ld	a,(OFST+5,sp)
5508  0078 88            	push	a
5509  0079 cd0145        	call	_ht1621_time
5511  007c 84            	pop	a
5512                     ; 51 	return;
5514  007d 205d          	jra	L02
5515  007f               L1463:
5516                     ; 54   t=t/60;
5518  007f 1e01          	ldw	x,(OFST+1,sp)
5519  0081 a63c          	ld	a,#60
5520  0083 62            	div	x,a
5521  0084 1f01          	ldw	(OFST+1,sp),x
5522                     ; 56   PB_ODR &= ~(1<<CS);
5524  0086 72195005      	bres	_PB_ODR,#4
5525                     ; 57   ht1621_send(0xa0, 3);
5527  008a aea003        	ldw	x,#40963
5528  008d cd0282        	call	_ht1621_send
5530                     ; 58   ht1621_send(0, 6);
5532  0090 ae0006        	ldw	x,#6
5533  0093 cd0282        	call	_ht1621_send
5535                     ; 59   ht1621_send(0, 8);
5537  0096 ae0008        	ldw	x,#8
5538  0099 cd0282        	call	_ht1621_send
5540                     ; 60   ht1621_send(0, 8);
5542  009c ae0008        	ldw	x,#8
5543  009f cd0282        	call	_ht1621_send
5545                     ; 61   ht1621_send(MINUS, 8);
5547  00a2 ae0208        	ldw	x,#520
5548  00a5 cd0282        	call	_ht1621_send
5550                     ; 62   ht1621_send(digits[(t%10)], 8);
5552  00a8 1e01          	ldw	x,(OFST+1,sp)
5553  00aa a60a          	ld	a,#10
5554  00ac 62            	div	x,a
5555  00ad 5f            	clrw	x
5556  00ae 97            	ld	xl,a
5557  00af d60000        	ld	a,(_digits,x)
5558  00b2 ae0008        	ldw	x,#8
5559  00b5 95            	ld	xh,a
5560  00b6 cd0282        	call	_ht1621_send
5562                     ; 63   t=t/10;
5564  00b9 1e01          	ldw	x,(OFST+1,sp)
5565  00bb a60a          	ld	a,#10
5566  00bd 62            	div	x,a
5567  00be 1f01          	ldw	(OFST+1,sp),x
5568                     ; 64   if (t>0)
5570  00c0 1e01          	ldw	x,(OFST+1,sp)
5571  00c2 270e          	jreq	L3463
5572                     ; 65 	ht1621_send(digits[(t)], 8);
5574  00c4 1e01          	ldw	x,(OFST+1,sp)
5575  00c6 d60000        	ld	a,(_digits,x)
5576  00c9 ae0008        	ldw	x,#8
5577  00cc 95            	ld	xh,a
5578  00cd cd0282        	call	_ht1621_send
5581  00d0 2006          	jra	L5463
5582  00d2               L3463:
5583                     ; 67 	ht1621_send(0,8);      
5585  00d2 ae0008        	ldw	x,#8
5586  00d5 cd0282        	call	_ht1621_send
5588  00d8               L5463:
5589                     ; 68   PB_ODR |= (1<<CS);
5591  00d8 72185005      	bset	_PB_ODR,#4
5592                     ; 70 }
5593  00dc               L02:
5596  00dc 85            	popw	x
5597  00dd 81            	ret
5653                     ; 72 void ht1621_hour(uint16_t t, uint8_t flag) {
5654                     	switch	.text
5655  00de               _ht1621_hour:
5657  00de 89            	pushw	x
5658  00df 88            	push	a
5659       00000001      OFST:	set	1
5662                     ; 74   d=(uint8_t)(t%60);
5664  00e0 a63c          	ld	a,#60
5665  00e2 62            	div	x,a
5666  00e3 5f            	clrw	x
5667  00e4 97            	ld	xl,a
5668  00e5 9f            	ld	a,xl
5669  00e6 6b01          	ld	(OFST+0,sp),a
5671                     ; 75   if (flag) {
5673  00e8 0d06          	tnz	(OFST+5,sp)
5674  00ea 270a          	jreq	L5763
5675                     ; 76 	ht1621_time(t,flag);
5677  00ec 7b06          	ld	a,(OFST+5,sp)
5678  00ee 88            	push	a
5679  00ef 1e03          	ldw	x,(OFST+2,sp)
5680  00f1 ad52          	call	_ht1621_time
5682  00f3 84            	pop	a
5683                     ; 77 	return;
5685  00f4 204c          	jra	L42
5686  00f6               L5763:
5687                     ; 80   PB_ODR &= ~(1<<CS);
5689  00f6 72195005      	bres	_PB_ODR,#4
5690                     ; 81   ht1621_send(0xa0, 3);
5692  00fa aea003        	ldw	x,#40963
5693  00fd cd0282        	call	_ht1621_send
5695                     ; 82   ht1621_send(0, 6);
5697  0100 ae0006        	ldw	x,#6
5698  0103 cd0282        	call	_ht1621_send
5700                     ; 83   ht1621_send(digits[(d%10)], 8);
5702  0106 7b01          	ld	a,(OFST+0,sp)
5703  0108 5f            	clrw	x
5704  0109 97            	ld	xl,a
5705  010a a60a          	ld	a,#10
5706  010c cd0000        	call	c_smodx
5708  010f d60000        	ld	a,(_digits,x)
5709  0112 ae0008        	ldw	x,#8
5710  0115 95            	ld	xh,a
5711  0116 cd0282        	call	_ht1621_send
5713                     ; 84   ht1621_send(digits[(d/10)], 8);
5715  0119 7b01          	ld	a,(OFST+0,sp)
5716  011b 5f            	clrw	x
5717  011c 97            	ld	xl,a
5718  011d a60a          	ld	a,#10
5719  011f cd0000        	call	c_sdivx
5721  0122 d60000        	ld	a,(_digits,x)
5722  0125 ae0008        	ldw	x,#8
5723  0128 95            	ld	xh,a
5724  0129 cd0282        	call	_ht1621_send
5726                     ; 85   ht1621_send(MINUS, 8);
5728  012c ae0208        	ldw	x,#520
5729  012f cd0282        	call	_ht1621_send
5731                     ; 86   ht1621_send(0, 8);
5733  0132 ae0008        	ldw	x,#8
5734  0135 cd0282        	call	_ht1621_send
5736                     ; 87   ht1621_send(0, 8);      
5738  0138 ae0008        	ldw	x,#8
5739  013b cd0282        	call	_ht1621_send
5741                     ; 88   PB_ODR |= (1<<CS);
5743  013e 72185005      	bset	_PB_ODR,#4
5744                     ; 90 }
5745  0142               L42:
5748  0142 5b03          	addw	sp,#3
5749  0144 81            	ret
5804                     ; 92 void ht1621_time(uint16_t t, uint8_t flag) {
5805                     	switch	.text
5806  0145               _ht1621_time:
5808  0145 89            	pushw	x
5809  0146 88            	push	a
5810       00000001      OFST:	set	1
5813                     ; 94   d=(uint8_t)(t%60);
5815  0147 a63c          	ld	a,#60
5816  0149 62            	div	x,a
5817  014a 5f            	clrw	x
5818  014b 97            	ld	xl,a
5819  014c 9f            	ld	a,xl
5820  014d 6b01          	ld	(OFST+0,sp),a
5822                     ; 95   t=t/60;
5824  014f 1e02          	ldw	x,(OFST+1,sp)
5825  0151 a63c          	ld	a,#60
5826  0153 62            	div	x,a
5827  0154 1f02          	ldw	(OFST+1,sp),x
5828                     ; 97   PB_ODR &= ~(1<<CS);
5830  0156 72195005      	bres	_PB_ODR,#4
5831                     ; 98   ht1621_send(0xa0, 3);
5833  015a aea003        	ldw	x,#40963
5834  015d cd0282        	call	_ht1621_send
5836                     ; 99   ht1621_send(0, 6);
5838  0160 ae0006        	ldw	x,#6
5839  0163 cd0282        	call	_ht1621_send
5841                     ; 100   ht1621_send(digits[(d%10)], 8);
5843  0166 7b01          	ld	a,(OFST+0,sp)
5844  0168 5f            	clrw	x
5845  0169 97            	ld	xl,a
5846  016a a60a          	ld	a,#10
5847  016c cd0000        	call	c_smodx
5849  016f d60000        	ld	a,(_digits,x)
5850  0172 ae0008        	ldw	x,#8
5851  0175 95            	ld	xh,a
5852  0176 cd0282        	call	_ht1621_send
5854                     ; 101   ht1621_send(digits[(d/10)], 8);
5856  0179 7b01          	ld	a,(OFST+0,sp)
5857  017b 5f            	clrw	x
5858  017c 97            	ld	xl,a
5859  017d a60a          	ld	a,#10
5860  017f cd0000        	call	c_sdivx
5862  0182 d60000        	ld	a,(_digits,x)
5863  0185 ae0008        	ldw	x,#8
5864  0188 95            	ld	xh,a
5865  0189 cd0282        	call	_ht1621_send
5867                     ; 102   if (flag)
5869  018c 0d06          	tnz	(OFST+5,sp)
5870  018e 2708          	jreq	L5273
5871                     ; 103 	ht1621_send(MINUS, 8);
5873  0190 ae0208        	ldw	x,#520
5874  0193 cd0282        	call	_ht1621_send
5877  0196 2006          	jra	L7273
5878  0198               L5273:
5879                     ; 105 	ht1621_send(0,8);
5881  0198 ae0008        	ldw	x,#8
5882  019b cd0282        	call	_ht1621_send
5884  019e               L7273:
5885                     ; 106   ht1621_send(digits[(t%10)], 8);
5887  019e 1e02          	ldw	x,(OFST+1,sp)
5888  01a0 a60a          	ld	a,#10
5889  01a2 62            	div	x,a
5890  01a3 5f            	clrw	x
5891  01a4 97            	ld	xl,a
5892  01a5 d60000        	ld	a,(_digits,x)
5893  01a8 ae0008        	ldw	x,#8
5894  01ab 95            	ld	xh,a
5895  01ac cd0282        	call	_ht1621_send
5897                     ; 107   t=t/10;
5899  01af 1e02          	ldw	x,(OFST+1,sp)
5900  01b1 a60a          	ld	a,#10
5901  01b3 62            	div	x,a
5902  01b4 1f02          	ldw	(OFST+1,sp),x
5903                     ; 108   if (t>0)
5905  01b6 1e02          	ldw	x,(OFST+1,sp)
5906  01b8 270e          	jreq	L1373
5907                     ; 109 	ht1621_send(digits[(t)], 8);
5909  01ba 1e02          	ldw	x,(OFST+1,sp)
5910  01bc d60000        	ld	a,(_digits,x)
5911  01bf ae0008        	ldw	x,#8
5912  01c2 95            	ld	xh,a
5913  01c3 cd0282        	call	_ht1621_send
5916  01c6 2006          	jra	L3373
5917  01c8               L1373:
5918                     ; 111 	ht1621_send(0,8);      
5920  01c8 ae0008        	ldw	x,#8
5921  01cb cd0282        	call	_ht1621_send
5923  01ce               L3373:
5924                     ; 112   PB_ODR |= (1<<CS);
5926  01ce 72185005      	bset	_PB_ODR,#4
5927                     ; 114 }
5930  01d2 5b03          	addw	sp,#3
5931  01d4 81            	ret
5996                     ; 116 void ht1621_print(int num) {
5997                     	switch	.text
5998  01d5               _ht1621_print:
6000  01d5 89            	pushw	x
6001  01d6 5204          	subw	sp,#4
6002       00000004      OFST:	set	4
6005                     ; 117     uint16_t value=get_abs(num);
6007  01d8 9c            	rvf
6008  01d9 a30000        	cpw	x,#0
6009  01dc 2e03          	jrsge	L23
6010  01de 50            	negw	x
6011  01df 2000          	jra	L43
6012  01e1               L23:
6013  01e1               L43:
6014  01e1 1f03          	ldw	(OFST-1,sp),x
6016                     ; 118     uint8_t first=1;
6018  01e3 a601          	ld	a,#1
6019  01e5 6b02          	ld	(OFST-2,sp),a
6021                     ; 120 	if (num <= 0) 
6023  01e7 9c            	rvf
6024  01e8 1e05          	ldw	x,(OFST+1,sp)
6025  01ea 2c03          	jrsgt	L7673
6026                     ; 121 	  ht1621_clear();
6028  01ec cd0024        	call	_ht1621_clear
6030  01ef               L7673:
6031                     ; 123 	PB_ODR &= ~(1<<CS);
6033  01ef 72195005      	bres	_PB_ODR,#4
6034  01f3               L1773:
6035                     ; 125         uint8_t rm=value % 10;
6037  01f3 1e03          	ldw	x,(OFST-1,sp)
6038  01f5 a60a          	ld	a,#10
6039  01f7 62            	div	x,a
6040  01f8 5f            	clrw	x
6041  01f9 97            	ld	xl,a
6042  01fa 01            	rrwa	x,a
6043  01fb 6b01          	ld	(OFST-3,sp),a
6044  01fd 02            	rlwa	x,a
6046                     ; 127 		if (first) {
6048  01fe 0d02          	tnz	(OFST-2,sp)
6049  0200 271b          	jreq	L7773
6050                     ; 128 		  ht1621_send(0xa0, 3);
6052  0202 aea003        	ldw	x,#40963
6053  0205 ad7b          	call	_ht1621_send
6055                     ; 129 		  ht1621_send(0, 6);
6057  0207 ae0006        	ldw	x,#6
6058  020a ad76          	call	_ht1621_send
6060                     ; 130 		  ht1621_send(digits[rm], 8);		 
6062  020c 7b01          	ld	a,(OFST-3,sp)
6063  020e 5f            	clrw	x
6064  020f 97            	ld	xl,a
6065  0210 d60000        	ld	a,(_digits,x)
6066  0213 ae0008        	ldw	x,#8
6067  0216 95            	ld	xh,a
6068  0217 ad69          	call	_ht1621_send
6070                     ; 131 		  first=0;
6072  0219 0f02          	clr	(OFST-2,sp)
6075  021b 200d          	jra	L1004
6076  021d               L7773:
6077                     ; 133 		  ht1621_send(digits[rm], 8);		 
6079  021d 7b01          	ld	a,(OFST-3,sp)
6080  021f 5f            	clrw	x
6081  0220 97            	ld	xl,a
6082  0221 d60000        	ld	a,(_digits,x)
6083  0224 ae0008        	ldw	x,#8
6084  0227 95            	ld	xh,a
6085  0228 ad58          	call	_ht1621_send
6087  022a               L1004:
6088                     ; 135         value=value/10;
6090  022a 1e03          	ldw	x,(OFST-1,sp)
6091  022c a60a          	ld	a,#10
6092  022e 62            	div	x,a
6093  022f 1f03          	ldw	(OFST-1,sp),x
6095                     ; 136     } while (value>0);  
6097  0231 1e03          	ldw	x,(OFST-1,sp)
6098  0233 26be          	jrne	L1773
6099                     ; 138 	if (num < 0) 
6101  0235 9c            	rvf
6102  0236 1e05          	ldw	x,(OFST+1,sp)
6103  0238 2e05          	jrsge	L3004
6104                     ; 139 	  ht1621_send(MINUS,8);  
6106  023a ae0208        	ldw	x,#520
6107  023d ad43          	call	_ht1621_send
6109  023f               L3004:
6110                     ; 142 	PB_ODR |=  (1<<CS);
6112  023f 72185005      	bset	_PB_ODR,#4
6113                     ; 143 }
6116  0243 5b06          	addw	sp,#6
6117  0245 81            	ret
6162                     ; 145 void ht1621_send_data(uint8_t adr, uint8_t value) {
6163                     	switch	.text
6164  0246               _ht1621_send_data:
6166  0246 89            	pushw	x
6167       00000000      OFST:	set	0
6170                     ; 146 	adr <<=2;
6172  0247 0801          	sll	(OFST+1,sp)
6173  0249 0801          	sll	(OFST+1,sp)
6174                     ; 147     PB_ODR &= ~(1<<CS);
6176  024b 72195005      	bres	_PB_ODR,#4
6177                     ; 148     ht1621_send(0xa0, 3);
6179  024f aea003        	ldw	x,#40963
6180  0252 ad2e          	call	_ht1621_send
6182                     ; 149     ht1621_send(adr, 6);
6184  0254 7b01          	ld	a,(OFST+1,sp)
6185  0256 ae0006        	ldw	x,#6
6186  0259 95            	ld	xh,a
6187  025a ad26          	call	_ht1621_send
6189                     ; 150     ht1621_send(value, 8);
6191  025c 7b02          	ld	a,(OFST+2,sp)
6192  025e ae0008        	ldw	x,#8
6193  0261 95            	ld	xh,a
6194  0262 ad1e          	call	_ht1621_send
6196                     ; 151     PB_ODR |=  (1<<CS);
6198  0264 72185005      	bset	_PB_ODR,#4
6199                     ; 152 }
6202  0268 85            	popw	x
6203  0269 81            	ret
6239                     ; 154 void ht1621_send_cmd(uint8_t cmd) {
6240                     	switch	.text
6241  026a               _ht1621_send_cmd:
6243  026a 88            	push	a
6244       00000000      OFST:	set	0
6247                     ; 155 	PB_ODR &= ~(1<<CS);
6249  026b 72195005      	bres	_PB_ODR,#4
6250                     ; 156 	ht1621_send(0x80,4);
6252  026f ae8004        	ldw	x,#32772
6253  0272 ad0e          	call	_ht1621_send
6255                     ; 157 	ht1621_send(cmd,8);
6257  0274 7b01          	ld	a,(OFST+1,sp)
6258  0276 ae0008        	ldw	x,#8
6259  0279 95            	ld	xh,a
6260  027a ad06          	call	_ht1621_send
6262                     ; 158 	PB_ODR |=  (1<<CS);
6264  027c 72185005      	bset	_PB_ODR,#4
6265                     ; 159 }
6268  0280 84            	pop	a
6269  0281 81            	ret
6322                     ; 161 void ht1621_send(uint8_t data, uint8_t len) {
6323                     	switch	.text
6324  0282               _ht1621_send:
6326  0282 89            	pushw	x
6327  0283 88            	push	a
6328       00000001      OFST:	set	1
6331                     ; 163 	for(i=0;i<len;i++) {
6333  0284 0f01          	clr	(OFST+0,sp)
6336  0286 2021          	jra	L7704
6337  0288               L3704:
6338                     ; 164         PB_ODR=(data & 0x80) ? PB_ODR | (1<<DATA) : PB_ODR & ~(1<<DATA);
6340  0288 7b02          	ld	a,(OFST+1,sp)
6341  028a a580          	bcp	a,#128
6342  028c 2707          	jreq	L44
6343  028e c65005        	ld	a,_PB_ODR
6344  0291 aa40          	or	a,#64
6345  0293 2005          	jra	L64
6346  0295               L44:
6347  0295 c65005        	ld	a,_PB_ODR
6348  0298 a4bf          	and	a,#191
6349  029a               L64:
6350  029a c75005        	ld	_PB_ODR,a
6351                     ; 165 		PB_ODR &= ~(1<<WR);
6353  029d 721b5005      	bres	_PB_ODR,#5
6354                     ; 166 		PB_ODR |= (1<<WR);
6356  02a1 721a5005      	bset	_PB_ODR,#5
6357                     ; 167         data=(data<<1);
6359  02a5 0802          	sll	(OFST+1,sp)
6360                     ; 163 	for(i=0;i<len;i++) {
6362  02a7 0c01          	inc	(OFST+0,sp)
6364  02a9               L7704:
6367  02a9 7b01          	ld	a,(OFST+0,sp)
6368  02ab 1103          	cp	a,(OFST+2,sp)
6369  02ad 25d9          	jrult	L3704
6370                     ; 169 }
6373  02af 5b03          	addw	sp,#3
6374  02b1 81            	ret
6399                     	xdef	_ht1621_off
6400                     	xdef	_ht1621_send_data
6401                     	xdef	_ht1621_send
6402                     	xdef	_ht1621_send_cmd
6403                     	xdef	_digits
6404                     	xdef	_ht1621_print_num
6405                     	xdef	_ht1621_send_digit
6406                     	xdef	_ht1621_init
6407                     	xdef	_ht1621_clear
6408                     	xdef	_ht1621_print
6409                     	xdef	_ht1621_time
6410                     	xdef	_ht1621_hour
6411                     	xdef	_ht1621_minute
6412                     	xref.b	c_x
6431                     	xref	c_sdivx
6432                     	xref	c_smodx
6433                     	end
