   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.12.6 - 16 Dec 2021
   3                     ; Generator (Limited) V4.5.4 - 16 Dec 2021
5154                     ; 8 main()
5154                     ; 9 {
5156                     	switch	.text
5157  0000               _main:
5161                     ; 10   _fctcpy('F');
5163  0000 a646          	ld	a,#70
5164  0002 cd0000        	call	__fctcpy
5166                     ; 12     CLK_DIVR = 4;         	// SYSCLK=1MHz
5168  0005 350450c0      	mov	_CLK_DIVR,#4
5169                     ; 13 	CLK_PCKENR1 |= 0x04;    // Enable TIM4
5171  0009 721450c3      	bset	_CLK_PCKENR1,#2
5172                     ; 14 	CLK_PCKENR1 |= 0x01;	// Enable TIM2		
5174  000d 721050c3      	bset	_CLK_PCKENR1,#0
5175                     ; 18     PA_DDR = 0xFF; PA_CR1 = 0xFF; PA_ODR = 0;
5177  0011 35ff5002      	mov	_PA_DDR,#255
5180  0015 35ff5003      	mov	_PA_CR1,#255
5183  0019 725f5000      	clr	_PA_ODR
5184                     ; 20     PB_DDR = 0xFF; PB_CR1 = 0xFF; PB_ODR = 0;
5186  001d 35ff5007      	mov	_PB_DDR,#255
5189  0021 35ff5008      	mov	_PB_CR1,#255
5192  0025 725f5005      	clr	_PB_ODR
5193                     ; 22     PC_DDR = 0xFF; PC_CR1 = 0xFF; PC_ODR = 0;
5195  0029 35ff500c      	mov	_PC_DDR,#255
5198  002d 35ff500d      	mov	_PC_CR1,#255
5201  0031 725f500a      	clr	_PC_ODR
5202                     ; 24     PD_DDR = 0xFF; PD_CR1 = 0xFF; PD_ODR = 0;
5204  0035 35ff5011      	mov	_PD_DDR,#255
5207  0039 35ff5012      	mov	_PD_CR1,#255
5210  003d 725f500f      	clr	_PD_ODR
5211                     ; 26     PE_DDR = 0xFF; PE_CR1 = 0xFF; PE_ODR = 0;
5213  0041 35ff5016      	mov	_PE_DDR,#255
5216  0045 35ff5017      	mov	_PE_CR1,#255
5219  0049 725f5014      	clr	_PE_ODR
5220                     ; 28     PF_DDR = 0xFF; PF_CR1 = 0xFF; PF_ODR = 0;
5222  004d 35ff501b      	mov	_PF_DDR,#255
5225  0051 35ff501c      	mov	_PF_CR1,#255
5228  0055 725f5019      	clr	_PF_ODR
5229                     ; 32 	PB_DDR = ((1<<CS) | (1<<WR) | (1<<DATA)); // Push-Pull Mode
5231  0059 35705007      	mov	_PB_DDR,#112
5232                     ; 33     PB_CR1 = ((1<<CS) | (1<<WR) | (1<<DATA)); //
5234  005d 35705008      	mov	_PB_CR1,#112
5235                     ; 34     PB_CR2 = ((1<<CS) | (1<<WR) | (1<<DATA)); // Speed up 10 MHz
5237  0061 35705009      	mov	_PB_CR2,#112
5238                     ; 36 	PB_DDR &= ~((1<<ENC_L) | (1<<ENC_R));
5240  0065 c65007        	ld	a,_PB_DDR
5241  0068 a4fa          	and	a,#250
5242  006a c75007        	ld	_PB_DDR,a
5243                     ; 37     PB_CR1 |=  ((1<<ENC_L) | (1<<ENC_R));
5245  006d c65008        	ld	a,_PB_CR1
5246  0070 aa05          	or	a,#5
5247  0072 c75008        	ld	_PB_CR1,a
5248                     ; 39 	PB_DDR &= ~(1<<ENC_BTN);    // pullup
5250  0075 72135007      	bres	_PB_DDR,#1
5251                     ; 40     PB_CR1 |= (1<<ENC_BTN);
5253  0079 72125008      	bset	_PB_CR1,#1
5254                     ; 43     TIM4_SR1    = 0x0;                       // Clear Pending Bit
5256  007d 725f52e5      	clr	_TIM4_SR1
5257                     ; 44     TIM4_CR1   = 0x0;                       // Clear TIM1_CR1
5259  0081 725f52e0      	clr	_TIM4_CR1
5260                     ; 45     TIM4_CR2   = 0x0;                       // Clear TIM1_CR2
5262  0085 725f52e1      	clr	_TIM4_CR2
5263                     ; 46     TIM4_PSCR  = 6;     					// Prescaler 2^6= 64
5265  0089 350652e8      	mov	_TIM4_PSCR,#6
5266                     ; 47     TIM4_ARR   = 119;   // (10^6)/64(prescaler) =15625 -> 0x7A12 -> freq Timer IRQ =1Hz
5268  008d 357752e9      	mov	_TIM4_ARR,#119
5269                     ; 48     TIM4_IER   = 0x01;                      // set UIE flag, enable interrupt
5271  0091 350152e4      	mov	_TIM4_IER,#1
5272                     ; 49 	TIM4_CNTR  =0;							// clear counter
5274  0095 725f52e7      	clr	_TIM4_CNTR
5275                     ; 50 	WFE_CR3   |= 0x4;						// enable event
5277  0099 721450a8      	bset	_WFE_CR3,#2
5278                     ; 53 	TIM2_CCER1|= 0x22; 	// CC2P,CC1P: Capture/compare 2 output polarity
5280  009d c6525b        	ld	a,_TIM2_CCER1
5281  00a0 aa22          	or	a,#34
5282  00a2 c7525b        	ld	_TIM2_CCER1,a
5283                     ; 54 	TIM2_CCMR1|= 0x01; 	// CC1 channel is configured as input, IC1 is mapped on TI1FP1
5285  00a5 72105259      	bset	_TIM2_CCMR1,#0
5286                     ; 55 	TIM2_CCMR2|= 0x01;	// CC2 channel is configured as input, IC2 is mapped on TI2FP2
5288  00a9 7210525a      	bset	_TIM2_CCMR2,#0
5289                     ; 56 	TIM2_SMCR |= 0x01; 	// encoder mode 1
5291  00ad 72105252      	bset	_TIM2_SMCR,#0
5292                     ; 57 	TIM2_ARRH=1;		// max value counter =500
5294  00b1 3501525f      	mov	_TIM2_ARRH,#1
5295                     ; 58 	TIM2_ARRL=0xf4;		// max value counter =500
5297  00b5 35f45260      	mov	_TIM2_ARRL,#244
5298                     ; 59 	TIM2_CNTRH=0;		// clear counter
5300  00b9 725f525c      	clr	_TIM2_CNTRH
5301                     ; 60 	TIM2_CNTRL=0;		// clear counter
5303  00bd 725f525d      	clr	_TIM2_CNTRL
5304                     ; 61 	TIM2_CR1|=0x1; 		// enable counter
5306  00c1 72105250      	bset	_TIM2_CR1,#0
5307                     ; 63 	main_loop();
5309  00c5 cd0000        	call	_main_loop
5311                     ; 64 }
5314  00c8 81            	ret
5327                     	xdef	_main
5328                     	xref	__fctcpy
5329                     	xref	_main_loop
5348                     	end
